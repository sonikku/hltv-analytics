using System;
using HltvParser.Models.Interfaces.Score;
using HltvParser.Models.Score;

namespace HltvParser.Api.Converters
{
    public static class ScoreConverter
    {
        public static IScore ConvertTextScore(string score)
        {
            score = score.Replace(" ", "");
            int dashPosition = score.IndexOf("-", StringComparison.InvariantCulture);
            short homeScore = short.Parse(score.Substring(0, dashPosition));
            short awayScore = short.Parse(score.Substring(dashPosition + 1));
            IScore model = new ScoreBase()
            {
                AwayScore = awayScore,
                HomeScore = homeScore
            };
            return model;
        }
    }
}