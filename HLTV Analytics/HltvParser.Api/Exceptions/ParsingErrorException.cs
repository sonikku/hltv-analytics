﻿using System;
using HltvParser.Core.Base;
using HltvParser.Core.Interfaces;

namespace HltvParser.Api.Exceptions
{
    internal class ParsingErrorException : Exception
    {
        public IElement Element { get; }
        
        public ParsingErrorException() : base()
        {
        }

        public ParsingErrorException(string message, IElement element = null) : base(message)
        {
            Element = element;
        }

        public ParsingErrorException(string message, Exception innerex, IElement element = null) 
            : base(message, innerex)
        {
            Element = element;
        }
    }
}