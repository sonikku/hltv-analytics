using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using HltvParser.Api.Contexts.Base;
using HltvParser.Api.Exceptions;
using HltvParser.Models;
using HltvParser.Models.Map;
using HltvParser.Models.Match;
using HltvParser.Models.Score;
using HltvParser.Models.Team;
using HltvParser.Core.Interfaces;
using HltvParser.Models.Interfaces.Match;
using HltvParser.Models.Interfaces.Score;
using HltvParser.Models.Interfaces.Team;

namespace HltvParser.Api.Contexts
{
    public class MatchesListContext : ContextData<List<IMatch>>
    {
        private const string DateFormat = "dddd - yyyy-MM-dd";
        private const string TimeFormat = "HH:mm";
    
        private const string MainContentClass = "mainContent";
        private const string UpcomingMatchesContainerClass = "upcomingMatchesContainer";
        private const string UpcomingMatchesSectionClass = "upcomingMatchesSection";
        private const string MatchDayHeadlineClass = "matchDayHeadline";
        private const string UpcomingMatchClass = "upcomingMatch";
        private const string MatchTimeClass = "matchTime";
        private const string MatchTeamClass = "matchTeam";
        private const string MatchLinkClass = "match a-reset";
        private const string MatchAnalyticsLinkClass = "matchAnalytics";
        private const string MatchInfoEmptyClass = "matchInfoEmpty";
        private const string LiveMatchesClass = "liveMatches";
        private const string LiveMatchClass = "liveMatch";
        private const string SummaryContainerClass = "summary-container";
        private const string MapRowClass = "map-row";
        
        protected override List<IMatch> HandleData()
        {
            var page = Tab
                .Document
                .GetElementsByClassName(MainContentClass)
                .FirstOrDefault();
            if (page == null)
                throw new ParsingErrorException("Root element is null");

            var list = new List<IMatch>();
            
            // Getting dated list wrappers
            var upcomingMatchesContainer = page
                .GetElementsByClassName(UpcomingMatchesContainerClass)
                .FirstOrDefault();
            if (upcomingMatchesContainer == null)
                throw new ParsingErrorException("Couldn't load upcoming matches container");

            var liveMatchesContainer = page
                .GetElementsByClassName(LiveMatchesClass)
                .FirstOrDefault();
            if (liveMatchesContainer != null)
            {
                list.AddRange(ParseLiveMatches(liveMatchesContainer));
            }
            
            list.AddRange(ParseUpcomingMatches(upcomingMatchesContainer));

            return list;
        }

        private IEnumerable<LiveMatch> ParseLiveMatches(IElement liveMatchesList)
        {
            var list = new List<LiveMatch>();

            var liveMatches = liveMatchesList
                .GetElementsByClassName(LiveMatchClass)
                .ToList();

            foreach (var element in liveMatches)
            {
                var liveMatch = ParseLiveMatch(element);
                list.Add(liveMatch);
            }

            return list;
        }

        private IEnumerable<IMatch> ParseUpcomingMatches(IElement upcomingMatchesContainer)
        {
            var dayLists = upcomingMatchesContainer
                .GetElementsByClassName(UpcomingMatchesSectionClass)
                .ToList();

            var matches = new List<IMatch>();

            foreach (var list in dayLists)
            {
                matches.AddRange(ParseMatchesForDay(list));
            }

            return matches;
        }

        private LiveMatch ParseLiveMatch(IElement liveMatchBox)
        {
            var (homeTeam, awayTeam) = ParseTeams(liveMatchBox);
            var matchUrl = ParseMatchUrl(liveMatchBox);
            var oddsUrl = ParseAnalyticsUrl(liveMatchBox);
            var scores = ParseScores(liveMatchBox);

            var match = new LiveMatch()
            {
                HomeTeam = homeTeam,
                AwayTeam = awayTeam,
                AnalyticsUrl = oddsUrl,
                Date = DateTime.UtcNow,
                Scores = new OverallScore()
                {
                    MapScores = scores
                },
                Url = matchUrl
            };

            return match;
        }

        private IEnumerable<MapScore> ParseScores(IElement liveMatchBox)
        {
            var summaryContainer = liveMatchBox
                .GetElementsByClassName(SummaryContainerClass)
                .FirstOrDefault();
            if (summaryContainer == null)
                return new List<MapScore>();

            var mapRows = summaryContainer
                .GetElementsByClassName(MapRowClass);

            var scores = new List<MapScore>();
            
            foreach (var map in mapRows)
            {
                var children = map.Children.ToList();
                byte.TryParse(children[0].TextContent, out var homeScore);
                byte.TryParse(children[2].TextContent, out var awayScore);
                var mapName = children[1].TextContent;
                var score = new MapScore()
                {
                    Map = new MapBase()
                    {
                        Name = mapName
                    },
                    AwayScore = awayScore,
                    HomeScore = homeScore
                };
                scores.Add(score);
            }

            return scores;
        }
        
        private IEnumerable<IMatch> ParseMatchesForDay(IElement list)
        {
            var matches = new List<IMatch>();
            
            var dateString = list
                .GetElementsByClassName(MatchDayHeadlineClass)
                .FirstOrDefault()?
                .TextContent;
            if (dateString == null)
                throw new ParsingErrorException("Couldn't parse matches date");

            var date = DateTime.ParseExact(
                    dateString, 
                    DateFormat, 
                    CultureInfo.InvariantCulture);

            var upcomingMatches = list
                .GetElementsByClassName(UpcomingMatchClass)
                .ToList();

            foreach (var matchElement in upcomingMatches)
            {
                var match = ParseMatch(matchElement, date);
                matches.Add(match);
            }

            return matches;
        }

        private IMatch ParseMatch(IElement matchElement, DateTime date)
        {
            // Recognizing match type
            var emptyBlock = matchElement
                .GetElementsByClassName(MatchInfoEmptyClass)
                .FirstOrDefault();
            return emptyBlock != null ? 
                ParseEmptyMatch(matchElement, emptyBlock, date) : 
                ParseUpcomingMatch(matchElement, date);
        }

        private DateTime ParseMatchDate(IElement matchElement, DateTime date)
        {
            var timeElement = matchElement
                .GetElementsByClassName(MatchTimeClass)
                .FirstOrDefault();
            var timeString = timeElement?.TextContent;
            if (timeString != null) date += TimeSpan.Parse(timeString);
            return date;
        }

        private (TeamBase, TeamBase) ParseTeams(IElement matchElement)
        {
            var teamBoxes = matchElement
                .GetElementsByClassName(MatchTeamClass)
                .ToList();
            if (teamBoxes.Count < 2)
                throw new ParsingErrorException(
                    "Couldn't parse teams: less than 2 teams.");

            var homeImg = teamBoxes[0]
                .GetElementsByTagName("img")
                .FirstOrDefault();
            var homeName = homeImg?
                .GetAttributeValue("title");
            var homeImageUrl = homeImg?
                .GetAttributeValue("src");

            var awayImg = teamBoxes[1]
                .GetElementsByTagName("img")
                .FirstOrDefault();
            var awayName = awayImg?
                .GetAttributeValue("title");
            var awayImageUrl = awayImg?
                .GetAttributeValue("src");

            var homeTeam = new TeamBase()
            {
                Name = homeName,
                ImageUrl = homeImageUrl
            };

            var awayTeam = new TeamBase()
            {
                Name = awayName,
                ImageUrl = awayImageUrl
            };

            return (homeTeam, awayTeam);
        }

        private string ParseMatchUrl(IElement matchElement)
        {
            return matchElement
                .GetElementsByClassName(MatchLinkClass)
                .FirstOrDefault()?
                .GetAttributeValue("href");
        }

        private string ParseAnalyticsUrl(IElement matchElement)
        {
            return matchElement
                .GetElementsByClassName(MatchAnalyticsLinkClass)
                .FirstOrDefault()?
                .GetAttributeValue("href");
        }
        
        private MatchBase ParseEmptyMatch(IElement matchElement, IElement emptyBlock, DateTime date)
        {
            var comment = emptyBlock
                .GetElementsByTagName("span")
                .FirstOrDefault()?
                .TextContent;

            var match = new MatchBase()
            {
                Date = ParseMatchDate(matchElement, date),
                Comment = comment ?? "",
                Url = ParseMatchUrl(matchElement)
            };

            return match;
        }

        private UpcomingMatch ParseUpcomingMatch(IElement matchElement, DateTime date)
        {
            var matchUrl = ParseMatchUrl(matchElement);
            var oddsUrl = ParseAnalyticsUrl(matchElement);
            var (homeTeam, awayTeam) = ParseTeams(matchElement);
            
            var match = new UpcomingMatch
            {
                HomeTeam = homeTeam,
                AwayTeam = awayTeam,
                Date = ParseMatchDate(matchElement, date),
                AnalyticsUrl = oddsUrl,
                Url = matchUrl
            };
            
            return match;
        }

        public MatchesListContext()
        {
            Address = "https://hltv.org/matches";
        }
    }
}