using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using HltvParser.Api.Auxiliary;
using HltvParser.Api.Contexts.Base;
using HltvParser.Models.Team;
using HltvParser.Core.Interfaces;
using HltvParser.Models.Interfaces.Player;
using HltvParser.Models.Interfaces.Team;
using HltvParser.Models.Player;

namespace HltvParser.Api.Contexts
{
    public class RankingsContext : ContextData<IEnumerable<ITeam>>
    {
        private const string TeamBoxClass = "ranked-team standard-box";
        private const string RankingHeaderClass = "ranking-header";
        private const string PositionClass = "position";
        private const string TeamLogoClass = "team-logo";
        private const string RelativeBoxClass = "relative";
        private const string NameClass = "name";
        private const string LineupConClass = "lineup-con";
        private const string PlayerHolderClass = "player-holder";
        private const string PointerClass = "pointer";
        private const string PlayerPictureClass = "playerPicture";
        private const string NickClass = "nick";
        private const string PointsClass = "points";

        private const string DefaultAddress = "https://hltv.org/ranking/teams";
        
        protected override IEnumerable<ITeam> HandleData()
        {
            var page = Tab.Document;
            var teamBoxes = page
                .GetElementsByClassName(TeamBoxClass);
            if (teamBoxes == null)
                throw new Exception("Couldn't parse ranked teams list");

            var list = new List<ITeam>();

            foreach (var box in teamBoxes)
            {
                var rankingHeader = box
                    .GetElementsByClassName(RankingHeaderClass)
                    .FirstOrDefault();
                if (rankingHeader == null)
                    continue;

                var position = ParsePosition(rankingHeader);
                var points = ParsePoints(rankingHeader);

                var teamLogo = rankingHeader
                    .GetElementsByClassName(TeamLogoClass)
                    .FirstOrDefault();
                var teamImgUrl = ParseImageUrl(teamLogo) ?? "";

                var nameBox = rankingHeader
                    .GetElementsByClassName(NameClass)
                    .FirstOrDefault();
                var name = nameBox?.TextContent;

                var lineupBox = box
                    .GetElementsByClassName(LineupConClass)
                    .FirstOrDefault();

                var team = new RankedTeam()
                {
                    Name = name,
                    WorldRank = position,
                    ImageUrl = teamImgUrl,
                    Players = ParsePlayers(lineupBox),
                    Points = points
                };
                
                list.Add(team);
            }

            return list;
        }

        private int ParsePoints(IElement rankingHeader)
        {
            var pointsSpan = rankingHeader
                .GetElementsByClassName(PointsClass)
                .FirstOrDefault();
            var pointsString = new string(pointsSpan?.TextContent.Where(char.IsDigit).ToArray());
            var points = 0;
            if (pointsString != null)
                points = int.Parse(pointsString);
            return points;
        }
        
        private int ParsePosition(IElement rankingHeader)
        {
            var positionString = rankingHeader
                .GetElementsByClassName(PositionClass)
                .FirstOrDefault()?
                .TextContent;
            if (positionString == null)
                throw new ArgumentException(nameof(positionString));
            positionString = positionString.Replace("#", "");
            var position = int.Parse(positionString);
            return position;
        }

        private string ParseImageUrl(IElement teamLogo)
        {
            var teamImg = teamLogo?
                .GetElementsByTagName("img")
                .FirstOrDefault();
            return teamImg?
                .GetAttributeValue("src");
        }

        private IEnumerable<IPlayer> ParsePlayers(IElement lineupCon)
        {
            var players = new List<MatchPlayer>();
            
            var playerHolders = lineupCon
                .GetElementsByClassName(PlayerHolderClass);

            foreach (var holder in playerHolders)
            {
                var linkElement = holder
                    .GetElementsByClassName(PointerClass)
                    .FirstOrDefault();
                var url = linkElement?
                    .GetAttributeValue("href");

                var playerImgElement = holder
                    .GetElementsByClassName(PlayerPictureClass)
                    .FirstOrDefault();
                var playerImgUrl = playerImgElement?
                    .GetAttributeValue("src");
                var playerName = playerImgElement?
                    .GetAttributeValue("title");

                var nickElement = holder
                    .GetElementsByClassName(NickClass)
                    .FirstOrDefault();
                var nick = nickElement?
                    .TextContent;

                var player = new MatchPlayer()
                {
                    Nickname = nick,
                    FullName = playerName,
                    ImageUrl = playerImgUrl,
                    Url = url,
                    PlayerId = PlayerIdExtractor.GetPlayerId(url)
                };
                players.Add(player);
            }

            return players;
        }
        
        public RankingsContext()
        {
            Address = DefaultAddress;
        }
    }
}