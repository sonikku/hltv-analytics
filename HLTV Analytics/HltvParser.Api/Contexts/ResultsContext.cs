using System.Collections.Generic;
using System.Linq;
using HltvParser.Api.Auxiliary;
using HltvParser.Api.Contexts.Base;
using HltvParser.Api.Converters;
using HltvParser.Api.Exceptions;
using HltvParser.Models.Match;
using HltvParser.Models.Team;
using HltvParser.Core.Base;
using HltvParser.Core.Interfaces;
using HltvParser.Models.Interfaces.Match;
using HltvParser.Models.Interfaces.Score;
using HltvParser.Models.Interfaces.Team;

namespace HltvParser.Api.Contexts
{
    public class ResultsContext : ContextData<IEnumerable<IMatch>>
    {
        #region Constants
        
        private const string ResultsClass = "results";
        private const string FeaturedResultsContainerClass = "big-results";
        private const string ResultConClass = "result-con";
        private const string ResultsHolder = "results-holder allres";
        private const string LinkClass = "a-reset";
        private const string TeamCellClass = "team-cell";
        private const string TeamClass = "team";
        private const string TeamLogoClass = "team-logo";
        private const string ResultScoreClass = "result-score";
        private const string EventNameClass = "event-name";
        private const string UnixTimeClass = "data-zonedgrouping-entry-unix";
        
        #endregion
        
        protected override IEnumerable<IMatch> HandleData()
        {
            var loaded = Tab
                .Document
                .GetElementsByClassName(ResultsClass)
                .TryGetFirst(out var page);
            if (!loaded)
                throw new ParsingErrorException("Couldn't load results wrapper.");

            var results = new List<IMatch>();
            
            // var featuredLoaded = page
            //     .GetElementsByClassName(FeaturedResultsContainerClass)
            //     .TryGetFirst(out var featured);
            // if (featuredLoaded)
            // {
            //     var featuredCons = featured
            //         .GetElementsByClassName(ResultConClass);
            //     results.AddRange(featuredCons.Select(GetCommonResult));
            // }

            var resultsWrapperLoaded = page
                .GetElementsByClassName(ResultsHolder)
                .TryGetFirst(out var resultsWrapper);
            if (!resultsWrapperLoaded)
                throw new ParsingErrorException("Couldn't load results wrapper");
            var cons = resultsWrapper
                .GetElementsByClassName(ResultConClass);
            results.AddRange(cons
                .Select(GetResult));

            return results;
        }

        private IMatch GetCommonResult(IElement resultCon)
        {
            var url = "";
            var gotUrl = resultCon
                .GetElementsByClassName(LinkClass)
                .TryGetFirst(out var urlElement);
            if (gotUrl)
                url = urlElement.GetAttributeValue("href");
            
            var teamCells = resultCon
                .GetElementsByClassName(TeamCellClass)
                .ToList();
            var gotHomeTeamNameBox = teamCells
                .TryGet(0, out var homeTeamNameBox);
            var gotAwayTeamNameBox = teamCells
                .TryGet(1, out var awayTeamNameBox);
            if (!(gotAwayTeamNameBox && gotHomeTeamNameBox))
                throw new ParsingErrorException("Couldn't get team names.");

            var gotHomeTeamNameElement = homeTeamNameBox
                .GetElementsByClassName(TeamClass)
                .TryGet(0, out var homeTeamNameElement);
            var gotAwayTeamNameElement = awayTeamNameBox
                .GetElementsByClassName(TeamClass)
                .TryGet(0, out var awayTeamNameElement);

            var homeTeamName = homeTeamNameElement.TextContent;
            var awayTeamName = awayTeamNameElement.TextContent;

            var gotHomeTeamImageUrl = homeTeamNameBox
                .GetElementsByClassName(TeamLogoClass)
                .TryGet(0, out var homeTeamImageUrlElement);
            var gotAwayTeamImageUrl = awayTeamNameBox
                .GetElementsByClassName(TeamLogoClass)
                .TryGet(0, out var awayTeamImageUrlElement);

            var homeTeamImageUrl = homeTeamImageUrlElement
                .GetAttributeValue("src");
            var awayTeamImageUrl = awayTeamImageUrlElement
                .GetAttributeValue("src");

            var scoreStringBox = resultCon
                .GetElementsByClassName(ResultScoreClass)
                .ToList();
            var gotScoreString = scoreStringBox
                .TryGetFirst(out var scoreStringElement);
            if (!gotScoreString)
                throw new ParsingErrorException("Couldn't get score string.");
            var scoreString = scoreStringElement.TextContent;
            var score = ScoreConverter.ConvertTextScore(scoreString);

            var gotHomeLogoUrl = teamCells[0]
                .GetElementsByTagName("img")
                .ToList()
                .TryGet(0, out var homeLogoUrlElement);
            var gotAwayLogoUrl = teamCells[0]
                .GetElementsByTagName("img")
                .ToList()
                .TryGet(0, out var awayLogoUrlElement);

            var tourneyNameBox = resultCon
                .GetElementsByClassName(EventNameClass)
                .ToList();
            var gotTourneyNameElement = tourneyNameBox
                .TryGetFirst(out var tourneyNameElement);
            if (!gotTourneyNameElement)
                throw new ParsingErrorException("Couldn't get tourney name.");
            var tourneyName = tourneyNameElement.TextContent;
            
            var homeTeam = new TeamBase()
            {
                Name = homeTeamName,
                ImageUrl = homeTeamImageUrl
            };
            var awayTeam = new TeamBase()
            {
                Name = awayTeamName,
                ImageUrl = awayTeamImageUrl
            };
            IMatch match = new Match()
            {
                Scores = score,
                AwayTeam = awayTeam,
                HomeTeam = homeTeam,
                Url = url
            };
            return match;
        }
        
        private IMatch GetResult(IElement resultCon)
        {
            var match = GetCommonResult(resultCon);
            
            var timestamp = resultCon.GetAttributeValue(UnixTimeClass);
            if (timestamp == null)
                throw new ParsingErrorException("Couldn't get unixtime.");
            double unixtime = double.Parse(timestamp);
            var dt = unixtime.ConvertTimeStampToDateTime();

            match.Date = dt;
            return match;
        }

        public ResultsContext()
        {
            Address = "https://hltv.org/results";
        }

        public ResultsContext(int offset)
        {
            Address = "https://hltv.org/results?offset=" + offset;
        }
    }
}