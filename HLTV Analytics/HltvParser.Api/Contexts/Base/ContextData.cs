using System;
using System.Threading.Tasks;
using HltvParser.Api.Exceptions;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Team;

namespace HltvParser.Api.Contexts.Base
{
    public abstract class ContextData<T> : ContextBase
    {
        public T Data { get; protected set; }

        protected abstract T HandleData();

        public virtual async Task<T> RetrieveDataAsync()
        {
            try
            {
                await Tab.LoadDocumentAsync(Address);
            }
            catch (Exception ex)
            {
                throw new ParsingErrorException("Couldn't load web page.", ex);
            }
            Data = HandleData();
            return Data;
        }
    }
}