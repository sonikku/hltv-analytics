﻿using HltvParser.Models.Enums;
using HltvParser.Core.Base;
using HltvParser.Core.Bridges.AngleSharp;

namespace HltvParser.Api.Contexts.Base
{
    public class ContextBase
    {
        protected string Address { get; set; }

        protected Tab Tab { get; } = new Tab();
    }
}