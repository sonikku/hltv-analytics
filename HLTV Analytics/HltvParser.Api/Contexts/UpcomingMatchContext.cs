﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HltvParser.Api.Auxiliary;
using HltvParser.Api.Contexts.Base;
using HltvParser.Api.Exceptions;
using HltvParser.Core.Interfaces;
using HltvParser.Models;
using HltvParser.Models.Interfaces.Player;
using HltvParser.Models.Map;
using HltvParser.Models.Match;
using HltvParser.Models.Player;
using HltvParser.Models.Score;
using HltvParser.Models.Team;

namespace HltvParser.Api.Contexts
{
    public class UpcomingMatchContext : ContextData<UpcomingMatch>
    {
        #region Constants
        
        private const string RootNameClass = "match-page";
        private const string TeamsBoxClass = "standard-box teamsBox";
        private const string TeamClass = "team";
        private const string TeamNameClass = "teamName";
        private const string AnalyticsCenterContainerClass = "matchpage-analytics-center-container";
        private const string LogoClass = "logo";
        private const string DateClass = "date";
        private const string GridMapsClass = "g-grid maps";
        private const string MapholderClass = "mapholder";
        private const string MapNameClass = "mapname";
        private const string ResultsPlayedClass = "results played";
        private const string TeamScoreClass = "results-team-score";
        private const string PlayersClass = "players";
        private const string PlayerImageClass = "player-image";
        private const string PlayerCompareClass = "player-compare";
        private const string PlayerClass = "player";
        private const string LineupsClass = "lineups";
        private const string PlayerNicknameClass = "text-ellipsis";
        private const string HomeTeamCountryImageClass = "team1";
        private const string AwayTeamCountryImageClass = "team2";

        #endregion

        protected override UpcomingMatch HandleData()
        {
            var match = new UpcomingMatch();
            
            var page = Tab
                .Document
                .GetElementsByClassName(RootNameClass)
                .FirstOrDefault();
            if (page == null)
                throw new ParsingErrorException("Root element is null");
            
            //Getting teams
            var teamsBox = page
                .GetElementsByClassName(TeamsBoxClass)
                .FirstOrDefault();
            if (teamsBox == null)
                throw new ParsingErrorException(
                    "There was no elements with 'standard-box teamsBox' selector",
                    page);
            
            // Getting players
            var playersBoxes = page
                .GetElementsByClassName(LineupsClass)
                .FirstOrDefault()?
                .GetElementsByClassName(PlayersClass)
                .ToList();

            //Loading teams information
            var teamsInfo = teamsBox
                .GetElementsByClassName(TeamClass)
                .ToList();
            CheckTeamsLength(teamsInfo, teamsBox);

            //Getting match date and time
            DateTime dt = ParseDate(teamsBox);

            //Getting maps list
            List<MapScore> scores = ParseScores(page);

            string analyticsUrl = GetAnalyticsUrl(page);


            match.Date = dt;
            match.Url = Tab.Address;
            match.AnalyticsUrl = analyticsUrl;
            match.Maps = scores.Select(i => i.Map).ToList();

            if (playersBoxes != null)
            {
                //Getting home team info
                var home = GenerateTeam(teamsInfo[0], playersBoxes[0]);
                var away = GenerateTeam(teamsInfo[1], playersBoxes[1]);
                
                match.AwayTeam = away;
                match.HomeTeam = home;
            }

            return match;
        }

        private MatchTeam GenerateTeam(IElement teamsInfo, 
            IElement playersBox)
        {
            //Parsing team name
            var name = teamsInfo
                .GetElementsByClassName(TeamNameClass)?
                .FirstOrDefault()?
                .TextContent;
            if (string.IsNullOrEmpty(name))
                throw new ParsingErrorException("Team name is null", teamsInfo);
            
            //Parsing team url
            var url = teamsInfo
                .GetElementsByTagName("a")?
                .FirstOrDefault()?
                .GetAttributeValue("href");
            if (string.IsNullOrEmpty(url))
                throw new ParsingErrorException("Team url is not provided", 
                    teamsInfo);
            
            //Parsing team logo url
            var logoUrl = teamsInfo
                .GetElementsByClassName(LogoClass)?
                .FirstOrDefault()?
                .GetAttributeValue("src");
            if (string.IsNullOrEmpty(logoUrl))
                throw new ParsingErrorException("Team logo url is not provided", 
                    teamsInfo);

            var homeTeamBox = teamsInfo
                .GetElementsByClassName(HomeTeamCountryImageClass)?
                .FirstOrDefault();
            var awayTeamBox = teamsInfo
                .GetElementsByClassName(AwayTeamCountryImageClass)?
                .FirstOrDefault();

            var countryImageUrl = "https://hltv.org";
            var country = "";

            if (homeTeamBox != null)
            {
                countryImageUrl += homeTeamBox.GetAttributeValue("src");
                country = homeTeamBox.GetAttributeValue("alt");
            }
                
            if (awayTeamBox != null)
            {
                countryImageUrl += awayTeamBox.GetAttributeValue("src");
                country = awayTeamBox.GetAttributeValue("alt");
            }

            // Parsing players
            var players = GeneratePlayers(playersBox);

            var team = new MatchTeam()
            {
                Name = name,
                Url = url,
                ImageUrl = logoUrl,
                Players = players,
                Country = new Country()
                {
                    Name = country,
                    ImageUrl = countryImageUrl
                }
            };
            return team;
        }

        private static bool CheckTeamsLength(ICollection teamsCollection, 
            IElement teamsBox)
        {
            if (teamsCollection.Count == 0)
                throw new ParsingErrorException(
                    "No teams in match",
                    teamsBox);
            if (teamsCollection.Count < 2)
                throw new ParsingErrorException(
                    "There are less than 2 teams in this match",
                    teamsBox);
            return true;
        }

        private List<IPlayer> GeneratePlayers(IElement playersBox)
        {
            var trArray = playersBox
                .GetElementsByTagName("tr")
                .ToList();

            if (trArray.Count < 2)
            {
                throw new ParsingErrorException("Couldn't parse players info table.");
            }

            var trImages = trArray[0]
                .GetElementsByTagName("img")
                .ToList();
            var trNicknames = trArray[1].Children.ToList();

            if (trImages.Count != trNicknames.Count)
            {
                throw new ParsingErrorException("Couldn't parse players info: " +
                                                "count of images and nicknames are different.");
            }

            var playersList = new List<IPlayer>();
            
            for (var i = 0; i < trImages.Count; i++)
            {
                string imageUrl = trImages[i].GetAttributeValue("src");
                string name = trImages[i].GetAttributeValue("alt");
                string nickname = trNicknames[i]?
                    .GetElementsByClassName(PlayerNicknameClass)
                    .FirstOrDefault()?
                    .TextContent;

                //var id = PlayerIdExtractor.GetPlayerId(url);

                var player = new MatchPlayer()
                {
                    //Url = url,
                    Nickname = nickname,
                    //PlayerId = id,
                    FullName = name,
                    ImageUrl = imageUrl
                };
                playersList.Add(player);
            }

            return playersList;
        }
        
        private DateTime ParseDate(IElement teamsBox)
        {
            var unixtimeString = teamsBox?.
                GetElementsByClassName(DateClass)?
                .FirstOrDefault()?
                .GetAttributeValue("data-unix");
            if (string.IsNullOrEmpty(unixtimeString))
                throw new ParsingErrorException("No date found for this match", 
                    teamsBox);
            
            double unixtime = double.Parse(unixtimeString);
            DateTime dt = unixtime.ConvertTimeStampToDateTime();
            return dt;
        }

        private List<MapBase> ParseMapList(IElement page)
        {
            var gridMaps = page?
                .GetElementsByClassName(GridMapsClass)?
                .FirstOrDefault();
            var mapsInfo = gridMaps?
                .GetElementsByClassName(MapholderClass);
            var maps = new List<MapBase>();
            if (mapsInfo != null)
            {
                foreach (var map in mapsInfo)
                {
                    string imageUrl = map
                        .GetElementsByTagName("img")?
                        .FirstOrDefault()?
                        .GetAttributeValue("src");
                    string mapName = map
                        .GetElementsByClassName(MapNameClass)?
                        .FirstOrDefault()?
                        .TextContent;
                    if (string.IsNullOrEmpty(mapName))
                        throw new Exception("Map name is null or empty.");
                    maps.Add(new Map()
                    {
                        Name = mapName,
                        ImageUrl = imageUrl
                    });
                }
            }

            return maps;
        }

        private List<MapScore> ParseScores(IElement page)
        {
            var gridMaps = page?
                .GetElementsByClassName(GridMapsClass)?
                .FirstOrDefault();
            var scoresInfo = gridMaps?
                .GetElementsByClassName(ResultsPlayedClass)
                .ToList();
            List<MapBase> maps = ParseMapList(page);
            var scores = new List<MapScore>();
            if (scoresInfo?.Count > 0)
            {
                for (var i = 0; i <  scoresInfo.Count; i++)
                {
                    var scoresArray = scoresInfo[i]
                        .GetElementsByClassName(TeamScoreClass)
                        .ToList();

                    bool parsedHome = 
                        short.TryParse(scoresArray[0].TextContent, out var homeScore);
                    bool parsedAway = 
                        short.TryParse(scoresArray[1].TextContent, out var awayScore);
                
                    scores.Add(new MapScore()
                    {
                        Map = maps[i],
                        HomeScore = homeScore,
                        AwayScore = awayScore
                    });
                }
            }
            else
            {
                foreach (var t in maps)
                {
                    scores.Add(new MapScore()
                    {
                        Map = t
                    });
                }
            }

            return scores;
        }

        private string GetAnalyticsUrl(IElement page)
        {
            return page
                .GetElementsByClassName(AnalyticsCenterContainerClass)?
                .FirstOrDefault()?
                .GetAttributeValue("href");
        }
        
        public UpcomingMatchContext(string address)
        {
            Address = address;
        }
    }
}