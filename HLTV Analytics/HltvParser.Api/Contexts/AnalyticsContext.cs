using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using HltvParser.Api.Contexts.Base;
using HltvParser.Api.Exceptions;
using HltvParser.Models.Interfaces.Analytics;
using HltvParser.Models.Analytics;
using HltvParser.Core.Exceptions;
using HltvParser.Core.Interfaces;

namespace HltvParser.Api.Contexts
{
    public class AnalyticsContext : ContextData<IAnalytics>
    {
        private const string MatchEndedClass = "match-ended";
        private const string MainWrapperClass = "colCon";
        private const string AnalyticsBackClass = "analytics-back";
        private const string HomeTeamHeader = "analytics-team-1 team";
        private const string AwayTeamHeader = "analytics-team-2 team";
        
        private enum TeamPosition
        {
            Home,
            Away
        }
        
        private readonly Dictionary<TeamPosition, string> _teams = new Dictionary<TeamPosition, string>()
        {
            {TeamPosition.Home, "team1"},
            {TeamPosition.Away, "team2"}
        };

        protected override IAnalytics HandleData()
        {
            CheckIfMatchOver();
            
            var matchAnalytics = new Analytics();

            var page = Tab
                .Document
                .GetElementsByClassName(MainWrapperClass)
                .FirstOrDefault();
            if (page == null)
                throw new ParsingErrorException("Couldn't load main wrapper");
            
            // Getting match URL
            var matchUrl = Tab
                .Document
                .GetElementsByClassName(AnalyticsBackClass)?
                .FirstOrDefault()?
                .GetElementsByTagName("a")?
                .FirstOrDefault()?
                .GetAttributeValue("href");
            matchAnalytics.MatchUrl = matchUrl;
            matchAnalytics.Url = Tab.Address;
            
            // Getting teams
            var homeTeamContainer = Tab
                .Document
                .GetElementsByClassName(HomeTeamHeader)?
                .FirstOrDefault();
            var awayTeamContainer = 
                Tab
                .Document
                .GetElementsByClassName(AwayTeamHeader)?
                .FirstOrDefault();

            var homeTeam = new TeamAnalytics
            {
                Name = GetTeamName(homeTeamContainer), 
                ImageUrl = GetTeamImageUrl(homeTeamContainer)
            };

            var awayTeam = new TeamAnalytics()
            {
                Name = GetTeamName(awayTeamContainer),
                ImageUrl = GetTeamImageUrl(awayTeamContainer)
            };

            homeTeam.SummaryPro = GetProInsights(TeamPosition.Home);
            homeTeam.SummaryContra = GetContraInsights(TeamPosition.Home);
            awayTeam.SummaryPro = GetProInsights(TeamPosition.Away);
            awayTeam.SummaryContra = GetContraInsights(TeamPosition.Away);

            //homeTeam.Players = GetPlayersList(TeamPosition.Home);
            //awayTeam.Players = GetPlayersList(TeamPosition.Away);

            //homeTeam.HandicapData = GetHandicap(TeamPosition.Home);
            //awayTeam.HandicapData = GetHandicap(TeamPosition.Away);

            //homeTeam.MapStats = GetMapAnalytics(TeamPosition.Home);
            //awayTeam.MapStats = GetMapAnalytics(TeamPosition.Away);

            matchAnalytics.HomeTeamAnalytics = homeTeam;
            matchAnalytics.AwayTeamAnalytics = awayTeam;

            return matchAnalytics;
        }

        private void CheckIfMatchOver()
        {
            if (Tab
                .Document
                .GetElementsByClassName(MatchEndedClass)
                .FirstOrDefault() != null)
                throw new ContextException("Match is over, so analytics data can't be loaded.");
        }
        
        private string GetTeamName(IElement container)
        {
            return container?
                .GetElementsByClassName("team-name fadeUp")?
                .FirstOrDefault()?
                .GetElementsByClassName("name")?
                .FirstOrDefault()?
                .TextContent;
        }

        private string GetTeamImageUrl(IElement container)
        {
            return container?.
                GetElementsByClassName("team-logo-container")?.
                FirstOrDefault()?.
                GetElementsByTagName("img")?.
                FirstOrDefault()?.
                GetAttributeValue("src");
        }

        private string GetTeamCountry(IElement container)
        {
            return container?.GetElementsByClassName("flag")?
                .FirstOrDefault()?
                .GetAttributeValue("title");
        }

        private IEnumerable<IElement> GetSummary(TeamPosition team)
        {
            var summaryContainer = Tab
                .Document
                .GetElementsByClassName("analytics-insights-container " + _teams[team])
                .FirstOrDefault();
            var insights = summaryContainer?
                .GetElementsByClassName("analytics-insights-insight");
            return insights;
        }

        private IEnumerable<string> GetProInsights(TeamPosition team)
        {
            var wrapper = GetSummary(team);
            var proInsights = wrapper?
                .Where(i => i
                    .Children
                    .First()
                    .GetAttributeValue("class")
                    .Contains("in-favor"))
                .Select(k => k
                    .GetElementsByClassName("analytics-insights-info")?
                    .FirstOrDefault()?
                    .TextContent);
            return proInsights?.ToList();
        }

        private List<string> GetContraInsights(TeamPosition team)
        {
            var wrapper = GetSummary(team);
            var proInsights = wrapper?
                .Where(i => i
                    .Children
                    .First()
                    .GetAttributeValue("class")
                    .Contains("against"))?.
                Select(k => k
                    .GetElementsByClassName("analytics-insights-info")?
                    .FirstOrDefault()?
                    .TextContent);
            return proInsights?.ToList();
        }

        private IElement GetHeadToHeadContainer(TeamPosition team)
        {
            return Tab
                .Document
                .GetElementsByClassName("analytics-head-to-head-container " + _teams[team])
                .FirstOrDefault();
        }

        // private List<PlayerAnalytics> GetPlayersList(TeamPosition team)
        // {
        //     var players = new List<PlayerAnalytics>();
        //     IElement container = GetHeadToHeadContainer(team);
        //     IElement table = container.GetElementsByClassName("table-container").
        //         FirstOrDefault()?.GetElementsByTagName("tbody")?.FirstOrDefault();
        //     IHtmlCollection<IElement> playersTr = table?.GetElementsByTagName("tr");
        //     if (playersTr == null)
        //         throw new ParsingErrorException("Couldn't get players table");
        //     foreach (var element in playersTr)
        //     {
        //         // Getting base info
        //         string imageUrl = element.GetElementsByClassName("player-image")?.FirstOrDefault()
        //             ?.GetElementsByTagName("img")?.FirstOrDefault()?.GetAttribute("src");
        //         string nickName = element.GetElementsByClassName("player-nickname")?.FirstOrDefault()?.TextContent;
        //         IElement nameContainer = element.GetElementsByClassName("player-name")?.FirstOrDefault();
        //         string name = nameContainer?.TextContent;
        //         string country = nameContainer?.GetElementsByTagName("img")?.FirstOrDefault()?.GetAttribute("title");
        //         
        //         // Getting ratings
        //         string rating3Months = element.GetElementsByClassName("table-3-months")?.FirstOrDefault()?.TextContent;
        //         string ratingEvent = element.GetElementsByClassName("table-event")?.FirstOrDefault()?.TextContent;
        //
        //         var player = new PlayerAnalytics()
        //         {
        //             Nickname = nickName,
        //             FullName = name,
        //             PhotoUrl = imageUrl,
        //             Country = country,
        //             RatingFor3Months = (rating3Months != null && rating3Months != "-") ? 
        //                 float.Parse(rating3Months, CultureInfo.InvariantCulture) : 0,
        //             RatingForEvent = (ratingEvent != null && ratingEvent != "-") ? 
        //                 float.Parse(ratingEvent, CultureInfo.InvariantCulture) : 0
        //         };
        //         players.Add(player);
        //     }
        //
        //     return players;
        // }

        private Handicap GetHandicap(TeamPosition team)
        {
            var handicap = new Handicap();
            IElement handicapContainer =
                Tab
                .Document
                .GetElementsByClassName("analytics-handicap-table " + _teams[team])
                .FirstOrDefault();
            IElement tbody = handicapContainer?
                .GetElementsByTagName("tbody")
                .FirstOrDefault();
            IEnumerable<IElement> tr = tbody?.GetElementsByTagName("tr");
            handicap.Wins2To0 = GetHandicapPercents(tr, 0);
            handicap.Wins2To1 = GetHandicapPercents(tr, 1);
            handicap.Losses0To2 = GetHandicapPercents(tr, 2);
            handicap.Losses1To2 = GetHandicapPercents(tr, 3);
            handicap.Overtimes = GetHandicapPercents(tr, 4);
            
            var mapHandicapContainer = Tab
                .Document
                .GetElementsByClassName("analytics-handicap-map-container " + _teams[team])?
                .FirstOrDefault();
            var mapData = mapHandicapContainer?
                .GetElementsByClassName("analytics-handicap-map-data");
            var enumerable = mapData.ToList();
            if (mapData == null || enumerable.Count < 2)
                throw new ParsingErrorException("Maps handicap data contains less than 2 elements.");
            handicap.AverageRoundsLostInWins = float.Parse(enumerable[0]?
                    .GetElementsByTagName("div")?
                    .ToList()[0]
                    .TextContent ?? "0", 
                CultureInfo.InvariantCulture);
            handicap.AverageRoundsWonInLosses = float.Parse(enumerable[1]?
                    .GetElementsByTagName("div")?
                    .ToList()[0]
                    .TextContent ?? "0",
                CultureInfo.InvariantCulture);
            return handicap;
        }

        private float GetHandicapPercents(IEnumerable<IElement> tr, int index)
        {
            return GetPercents(tr?
                .ToList()[index]
                .GetElementsByClassName("handicap-data")?
                .FirstOrDefault()?
                .TextContent);
        }

        // private List<MapAnalytics> GetMapAnalytics(TeamPosition team)
        // {
        //     var mapsList = new List<MapAnalytics>();
        //
        //     // Getting picks/bans/wins data for one team
        //     
        //     int startPosition = team == TeamPosition.Home ? 0 : 1;
        //     
        //     var mapsDiv = ParserEngine.Document.GetElementById("maps");
        //     var mapsBody = mapsDiv.GetElementsByClassName("table-container gtSmartphone-only")?.
        //         FirstOrDefault()?.
        //         GetElementsByTagName("tbody").FirstOrDefault();
        //     var maps = mapsBody?.GetElementsByClassName("analytics-map-container");
        //     maps?.ToList().ForEach(i =>
        //     {
        //         var map = new MapAnalytics();
        //         string mapName = i.GetElementsByClassName("analytics-map-name")?.FirstOrDefault()?.TextContent;
        //         string mapImageUrl = i.GetElementsByTagName("img")?.FirstOrDefault()?.GetAttribute("src");
        //         map.Name = mapName;
        //         map.ImageUrl = mapImageUrl;
        //         mapsList.Add(map);
        //     });
        //
        //     var rows = mapsBody?.GetElementsByTagName("tr");
        //     if (rows == null || rows.Length == 0)
        //         throw new ParsingErrorException("Couldn't get map analytics.");
        //     int mapIterator = 0;
        //     for (int i = startPosition; i < rows?.Length; i += 2)
        //     {
        //         mapsList[mapIterator].FirstPicksRate = GetPercents(rows[i].
        //             GetElementsByClassName("analytics-map-stats-pick-percentage")?.FirstOrDefault()?.TextContent);
        //         mapsList[mapIterator].FirstBansRate = GetPercents(rows[i].
        //             GetElementsByClassName("analytics-map-stats-ban-percentage")?.FirstOrDefault()?.TextContent);
        //         mapsList[mapIterator].WinRate = GetPercents(rows[i].
        //             GetElementsByClassName("analytics-map-stats-win-percentage")?.FirstOrDefault()?.TextContent);
        //         mapsList[mapIterator].TimesPlayed = int.Parse(rows[i].
        //                                                           GetElementsByClassName("analytics-map-stats-played")?.FirstOrDefault()?.TextContent ?? "0");
        //         var comments = rows[i].GetElementsByClassName("comment").
        //             Select(k => k.TextContent).ToList();
        //         mapsList[mapIterator].Comments = comments;
        //         mapIterator++;
        //     }
        //     
        //     // Getting map handicaps
        //
        //     var handicapContainer =
        //         ParserEngine.Document.GetElementsByClassName("analytics-handicap-map-container " + _teams[team])?.
        //             FirstOrDefault();
        //
        //     var handicapsTableBody = handicapContainer?.GetElementsByClassName("table-container")?.
        //         FirstOrDefault()?.
        //         GetElementsByTagName("tbody").FirstOrDefault();
        //     var handicapsTr = handicapsTableBody?.GetElementsByTagName("tr");
        //     for (int i = 0; i < handicapsTr?.Length; i++)
        //     {
        //         var avgData = handicapsTr[i].GetElementsByClassName("analytics-handicap-map-data-avg");
        //         mapsList[i].AverageRoundsLostInWins = avgData[0].TextContent != "-" ? 
        //             float.Parse(avgData[0].TextContent, CultureInfo.InvariantCulture) : 0;
        //         mapsList[i].AverageRoundsWonInLosses = avgData[1].TextContent != "-" ?
        //             float.Parse(avgData[1].TextContent, CultureInfo.InvariantCulture) : 0;
        //     }
        //
        //     return mapsList;
        // }

        private float GetPercents(string number)
        {
            return float.Parse(number.Replace("%", ""), CultureInfo.InvariantCulture) / 100;
        }

        public AnalyticsContext(string address)
        {
            Address = address;
        }
    }
}