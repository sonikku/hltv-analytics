using System;
using static System.UInt32;

namespace HltvParser.Api.Auxiliary
{
    public static class PlayerIdExtractor
    {
        /// <summary>
        /// Gets player's ID from player's URL
        /// </summary>
        /// <param name="url">Player's URL</param>
        /// <returns></returns>
        internal static uint GetPlayerId(string url)
        {
            // URL like "/players/{id}/nickname"
            var parts = url.Split('/');
            if (parts.Length < 3)
            {
                throw new ArgumentException("Incorrect player URL format");
            }

            uint id;
            bool parsed = TryParse(parts[2], out id);

            if (parsed) return id;

            throw new ArgumentException("Couldn't parse player id");
        }
    }
}