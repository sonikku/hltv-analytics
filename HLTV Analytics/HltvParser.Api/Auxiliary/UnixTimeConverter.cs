﻿using System;

namespace HltvParser.Api.Auxiliary
{
    public static class UnixTimeConverter
    {
        public static DateTime ConvertTimeStampToDateTime(this double unixtime)
        {
            var dtDateTime = 
                new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixtime).ToLocalTime();
            return dtDateTime;
        }
    }
}