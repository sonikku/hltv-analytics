﻿using System;
using System.Globalization;
using HltvParser.Client.Resources;
using Xamarin.Forms;

namespace HltvParser.Client.Converters
{
    public class DateTimeOffsetToNowConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is TimeSpan date)) return "";
            var formatted = "";
            formatted += date.Days > 0 ? string.Format(TranslationStrings.DtDays, date.Days) + " " : "";
            formatted += (date.Hours > 0 || (date.Hours == 0 && date.Days > 0)) ? date.Hours + ":" : "";
            formatted += date.Minutes.ToString("00") + ":";
            formatted += date.Seconds.ToString("00");
            return formatted;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}