﻿using System;
using System.Globalization;
using System.Reflection;
using HltvParser.Client.Models.Analytics;
using Xamarin.Forms;

namespace HltvParser.Client.Converters.InsightsConverters
{
    public class InsightImageConverter : IValueConverter
    {
        private const string Path = "resource://HltvParser.Client.Assets.";
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var type = (InsightType)value;
            var path = Path;
            if (type == InsightType.Pro)
                path += "CheckboxFor.svg";
            else
                path += "CheckboxAgainst.svg";
            return path;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}