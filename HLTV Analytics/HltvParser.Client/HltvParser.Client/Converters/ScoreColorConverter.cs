﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace HltvParser.Client.Converters
{
    public class ScoreColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == null || values[1] == null)
                return Color.Black;
            var val1 = (short)values[0];
            var val2 = (short)values[1];

            if (val1 > val2)
                return Color.Green;
            return val2 > val1 ? Color.Red : Color.Black;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}