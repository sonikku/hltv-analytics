﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace HltvParser.Client.Converters
{
    public class StringWithDecimalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var paramString = parameter.ToString();
            var val = (int) value;
            return string.Format(paramString, val);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public class ParametrizedStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var paramString = parameter.ToString();
            var val = value.ToString();
            return string.Format(paramString, val);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}