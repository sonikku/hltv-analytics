﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace HltvParser.Client.Converters.ResultsConverters
{
    public class ResultDateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var date = (DateTime) value;
            return date.Year == 1 ? 
                Resources.TranslationStrings.ResultsFeatured :
                date.ToString(CultureInfo.CurrentUICulture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}