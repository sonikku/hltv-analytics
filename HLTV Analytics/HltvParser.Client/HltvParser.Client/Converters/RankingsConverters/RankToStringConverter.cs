﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace HltvParser.Client.Converters.RankingsConverters
{
    public class RankToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (int) value;
            return "Rank #" + val;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}