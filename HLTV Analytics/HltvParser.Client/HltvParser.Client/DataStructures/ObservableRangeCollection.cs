﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace HltvParser.Client.DataStructures
{
    public class ObservableRangeCollection<T> : ObservableCollection<T>
    {
        private const string CountString = "Count";
        private const string IndexerName = "Item[]";

        private readonly object _lockObj = new object();

        protected enum ProcessRangeAction
        {
            Add,
            Replace,
            Remove
        };

        public ObservableRangeCollection() : base() { }

        public ObservableRangeCollection(IEnumerable<T> collection) : base(collection) { }

        public ObservableRangeCollection(List<T> list) : base(list) { }

        protected virtual void ProcessRange(IEnumerable<T> collection, ProcessRangeAction action)
        {
            lock(_lockObj)
            {
                if (collection == null) 
                    throw new ArgumentNullException(nameof(collection));

                var items = collection as IList<T> ?? collection.ToList();
                if (!items.Any()) return;

                CheckReentrancy();
                if (action == ProcessRangeAction.Replace) Items.Clear();

                if (action == ProcessRangeAction.Remove)
                {
                    foreach (var item in items) Items.Remove(item);
                }
                else
                {
                    foreach (var item in items) Items.Add(item);
                }

                OnPropertyChanged(new PropertyChangedEventArgs(CountString));
                OnPropertyChanged(new PropertyChangedEventArgs(IndexerName));
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        public void AddRange(IEnumerable<T> collection)
        {
            ProcessRange(collection, ProcessRangeAction.Add);
        }

        public void ReplaceRange(IEnumerable<T> collection)
        {
            ProcessRange(collection, ProcessRangeAction.Replace);
        }

        public void RemoveRange(IEnumerable<T> collection)
        {
            ProcessRange(collection, ProcessRangeAction.Remove);
        }

    }
}