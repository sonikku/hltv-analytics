﻿using HltvParser.Client.Services;
using HltvParser.Facade.Implementations;
using Xamarin.Forms;

namespace HltvParser.Client
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();
            DependencyService.Register<HltvFacadeAdapter>();
            DependencyService.Register<HltvDirectFacade>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
