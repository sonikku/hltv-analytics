﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HltvParser.Client.Helpers
{
    [ContentProperty(nameof(Name))]
    public class SvgAssetHelper : IMarkupExtension
    {
        private const string AssetsFolder = "resource://HltvParser.Client.Assets.";
        
        public string Name { get; set; }
        
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Name == null) return null;

            var path = AssetsFolder + Name;

            return path;
        }
    }
}