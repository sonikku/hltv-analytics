﻿using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HltvParser.Client.Helpers
{
    [ContentProperty(nameof(Name))]
    public class AssetHelper : IMarkupExtension
    {
        private const string AssetsFolder = "HltvParser.Client.Assets.";
        
        public string Name { get; set; }
        
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Name == null) return null;

            var path = AssetsFolder + Name;
            
            var source =
                ImageSource.FromResource(
                    path,
                    typeof(AssetHelper).GetTypeInfo().Assembly);
            return source;
        }
    }
}