﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HltvParser.Client.Helpers
{
    [ContentProperty(nameof(Text))]
    public class TranslationHelper : IMarkupExtension
    {
        private CultureInfo ci;
        private const string ResourceId = "HltvParser.Client.Resources.TranslationStrings";

        private static readonly Lazy<ResourceManager> Manager =
            new Lazy<ResourceManager>(() => 
                new ResourceManager(
                    ResourceId, 
                    typeof(TranslationHelper).GetTypeInfo().Assembly));
        
        public string Text { get; set; }
        
        public TranslationHelper()
        {
            ci = CultureInfo.CurrentUICulture;
        }
        
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
            {
                return "";
            }

            if (ci.TwoLetterISOLanguageName != 
                CultureInfo.CurrentUICulture.TwoLetterISOLanguageName)
            {
                ci = CultureInfo.InvariantCulture;
            }

            var translation = Manager.Value.GetString(Text, ci) ?? Text;

            return translation;
        }
    }
}