﻿using HltvParser.Client.Views;
using Xamarin.Forms;

namespace HltvParser.Client
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(RankingsPage), typeof(RankingsPage));
            Routing.RegisterRoute(nameof(ResultsPage), typeof(ResultsPage));
            Routing.RegisterRoute(nameof(UpcomingMatchesPage), typeof(UpcomingMatchesPage));
            Routing.RegisterRoute(nameof(MatchPage), typeof(MatchPage));
        }
    }
}
