﻿using System.Linq;
using System.Threading.Tasks;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Match;
using HltvParser.Client.Models.Match.Interfaces;
using HltvParser.Facade.Implementations;
using HltvParser.Facade.Interfaces;
using HltvParser.Models.Enums;
using HltvParser.Models.Match;
using MatchBase = HltvParser.Client.Models.Match.MatchBase;
using UpcomingMatch = HltvParser.Client.Models.Match.UpcomingMatch;

namespace HltvParser.Client.Services
{
    public class HltvFacadeAdapter
    {
        private IHltvParserFacade _facade = new HltvDirectFacade();

        public async Task<UpcomingMatch> GetUpcomingMatch(string matchUrl, string analyticsUrl)
        {
            var matchData = await _facade.GetUpcomingMatchData(matchUrl);
            var analyticsData = await _facade.GetAnalyticsData(analyticsUrl);
            return new UpcomingMatch(matchData, analyticsData);
        }

        public async Task<ObservableRangeCollection<EndedMatch>> GetResults()
        {
            var resultsData = await _facade.GetResults();
            return new ObservableRangeCollection<EndedMatch>(resultsData
                .Select(i => new EndedMatch((Match)i)));
        }

        public async Task<ObservableRangeCollection<IMatch>> GetMatchesList()
        {
            var matchesData = await _facade.GetMatchesList();
            return new ObservableRangeCollection<IMatch>(matchesData
                .Select<HltvParser.Models.Interfaces.Match.IMatch, IMatch>(i =>
                {
                    switch (i.MatchType)
                    {
                        case MatchType.Upcoming:
                            return new ShortUpcomingMatch((HltvParser.Models.Match.UpcomingMatch) i);
                        case MatchType.Live:
                            return new ShortLiveMatch((HltvParser.Models.Match.LiveMatch) i);
                        case MatchType.Undefined:
                            return new MatchBase(i);
                        default:
                            return new MatchBase(i);
                    }
                })
            );
        }
    }
}