﻿<?xml version="1.0" encoding="utf-8"?>

<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:forms="clr-namespace:FFImageLoading.Svg.Forms;assembly=FFImageLoading.Svg.Forms"
             xmlns:helpers="clr-namespace:HltvParser.Client.Helpers;assembly=HltvParser.Client"
             xmlns:analytics="clr-namespace:HltvParser.Client.Models.Analytics;assembly=HltvParser.Client"
             xmlns:insightsConverters="clr-namespace:HltvParser.Client.Converters.InsightsConverters;assembly=HltvParser.Client"
             xmlns:converters="clr-namespace:HltvParser.Client.Converters;assembly=HltvParser.Client"
             xmlns:viewModels="clr-namespace:HltvParser.Client.ViewModels;assembly=HltvParser.Client"
             x:Class="HltvParser.Client.Views.MatchPage"
             Title="{helpers:TranslationHelper PageTitleMatch}">
    <ContentPage.Resources>
        <insightsConverters:InsightImageConverter x:Key="InsightConverter"/>
        <converters:ParametrizedStringConverter x:Key="ParametrizedStringConverter"/>
        <converters:DateTimeOffsetToNowConverter x:Key="DateTimeOffsetToNowConverter"/>
        <converters:InverseBoolConverter x:Key="InverseBoolConverter"/>
        <ResourceDictionary>
            <DataTemplate x:Key="InsightTemplate">
                <Grid
                    HeightRequest="64"
                    x:DataType="analytics:Insight">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*"/>
                        <ColumnDefinition Width="24"/>
                    </Grid.ColumnDefinitions>
                    <Label 
                        MaxLines="2"
                        TextColor="Black"
                        VerticalTextAlignment="Center"
                        Text="{Binding Comment}"/>
                    <forms:SvgCachedImage
                        Grid.Column="1"
                        HeightRequest="24"
                        WidthRequest="24"
                        Margin="0"
                        Source="{Binding Type, Converter={StaticResource InsightConverter}}"/>
                </Grid>
            </DataTemplate>
        </ResourceDictionary>
    </ContentPage.Resources>
    <ContentPage.Content>
        
        <RefreshView
            x:DataType="viewModels:MatchViewModel"
            Command="{Binding LoadMatchCommand}"
            IsRefreshing="{Binding IsBusy, Mode=OneWay}">
            <Grid
                IsVisible="{Binding IsBusy, Mode=OneWay, Converter={StaticResource InverseBoolConverter}}">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="0.5*"/>
                    <ColumnDefinition Width="0.5*"/>
                </Grid.ColumnDefinitions>
                
                <forms:SvgCachedImage
                    Grid.Row="0"
                    Grid.Column="0"
                    Source="{Binding Match.HomeTeam.Country.ImageUrl, Mode=OneWay}"
                    Aspect="AspectFill"/>
                
                <forms:SvgCachedImage
                    Grid.Row="0"
                    Grid.Column="1"
                    Source="{Binding Match.AwayTeam.Country.ImageUrl, Mode=OneWay}"
                    Aspect="AspectFill"/>
                
                <ContentView
                    Grid.Row="0"
                    Grid.Column="0"
                    Grid.ColumnSpan="2">
                    <ContentView.Background>
                        <LinearGradientBrush 
                            StartPoint="0,0.5"
                            EndPoint="1,0.5">
                            <GradientStop 
                                Color="#7f000000"
                                Offset="0.0"/>
                            <GradientStop 
                                Color="Black"
                                Offset="0.5"/>
                            <GradientStop 
                                Color="#7f000000"
                                Offset="1"/>
                        </LinearGradientBrush>
                    </ContentView.Background>
                </ContentView>
                
                <Grid
                    Padding="10"
                    Grid.Row="0"
                    Grid.Column="0"
                    Grid.ColumnSpan="2">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="32"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                    </Grid.RowDefinitions>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="32"/>
                        <ColumnDefinition Width="0.4*"/>
                        <ColumnDefinition Width="0.2*"/>
                        <ColumnDefinition Width="0.4*"/>
                        <ColumnDefinition Width="32"/>
                    </Grid.ColumnDefinitions>
                    
                    <forms:SvgCachedImage
                        Grid.Row="0"
                        Grid.Column="0"
                        VerticalOptions="Center"
                        WidthRequest="32"
                        HeightRequest="32"
                        Source="{Binding Match.HomeTeam.ImageUrl, Mode=OneWay}"/>
                    
                    <Label 
                        Grid.Row="1"
                        Grid.Column="0"
                        Grid.ColumnSpan="2"
                        TextColor="White"
                        VerticalOptions="End"
                        HorizontalTextAlignment="Start"
                        LineBreakMode="TailTruncation"
                        FontSize="Medium"
                        Text="{Binding Match.HomeTeam.Name, Mode=OneWay}"/>
                        
                    <Label
                        Grid.Row="1"
                        Grid.Column="2"
                        VerticalOptions="End"
                        HorizontalTextAlignment="Center"
                        FontSize="Title"
                        Text="VS"
                        TextColor="White"/>
                    
                    <Label
                        Grid.Row="2"
                        Grid.Column="0"
                        Grid.ColumnSpan="5"
                        VerticalOptions="End"
                        HorizontalTextAlignment="Center"
                        FontSize="Small"
                        TextColor="White"
                        Text="{Binding TimeLeft, Converter={StaticResource DateTimeOffsetToNowConverter}}"/>
                        
                    <Label 
                        Grid.Row="1"
                        Grid.Column="3"
                        Grid.ColumnSpan="2"
                        TextColor="White"
                        VerticalOptions="End"
                        HorizontalTextAlignment="End"
                        LineBreakMode="TailTruncation"
                        FontSize="Medium"
                        Text="{Binding Match.AwayTeam.Name, Mode=OneWay}"/>
                        
                        
                    <forms:SvgCachedImage
                        Grid.Row="0"
                        Grid.Column="4"
                        VerticalOptions="Center"
                        WidthRequest="32"
                        HeightRequest="32"
                        Source="{Binding Match.AwayTeam.ImageUrl, Mode=OneWay}"/>
                    
                </Grid>
                
                <ScrollView
                    Grid.Row="1"
                    Grid.Column="0"
                    Grid.ColumnSpan="2">
                    <StackLayout
                        Padding="10">
                        <Label TextColor="Black"
                               FontSize="Large"
                                Text="{Binding Match.HomeTeam.Name, Mode=OneWay,
                                Converter={StaticResource ParametrizedStringConverter},
                                ConverterParameter={helpers:TranslationHelper AnalyticsInsightsFor}}"/>
                        <FlexLayout
                            Direction="Column"
                            BindableLayout.ItemsSource="{Binding Match.HomeTeam.Analytics, Mode=OneWay}"
                            BindableLayout.ItemTemplate="{StaticResource InsightTemplate}"
                            />
                        <Label TextColor="Black"
                               FontSize="Large"
                               Text="{Binding Match.AwayTeam.Name, Mode=OneWay,
                                Converter={StaticResource ParametrizedStringConverter},
                                ConverterParameter={helpers:TranslationHelper AnalyticsInsightsFor}}"/>
                        <FlexLayout
                            Direction="Column"
                            BindableLayout.ItemsSource="{Binding Match.AwayTeam.Analytics, Mode=OneWay}"
                            BindableLayout.ItemTemplate="{StaticResource InsightTemplate}"/>
                    </StackLayout>
                </ScrollView>
            </Grid>
        </RefreshView>
        
    </ContentPage.Content>
</ContentPage>