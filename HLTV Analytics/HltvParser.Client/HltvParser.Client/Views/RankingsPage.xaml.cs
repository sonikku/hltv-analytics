﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HltvParser.Client.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HltvParser.Client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RankingsPage : ContentPage
    {
        private RankingsViewModel _viewModel;
        
        public RankingsPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new RankingsViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}