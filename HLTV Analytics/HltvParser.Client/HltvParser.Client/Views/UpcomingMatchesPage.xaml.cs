﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HltvParser.Client.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HltvParser.Client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpcomingMatchesPage : ContentPage
    {
        public UpcomingMatchesViewModel ViewModel { get; private set; }
        
        public UpcomingMatchesPage()
        {
            InitializeComponent();
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = ViewModel = new UpcomingMatchesViewModel();
            ViewModel.OnAppearing();
        }
    }
}