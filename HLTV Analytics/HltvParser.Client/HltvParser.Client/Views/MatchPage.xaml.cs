﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HltvParser.Client.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HltvParser.Client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [QueryProperty(nameof(MatchUrl), "matchUrl")]
    [QueryProperty(nameof(AnalyticsUrl), "analyticsUrl")]
    public partial class MatchPage : ContentPage
    {
        private string _matchUrl;
        private string _analyticsUrl;
        private MatchViewModel _viewModel;

        public string MatchUrl
        {
            get => _matchUrl;
            set => _matchUrl = Uri.UnescapeDataString(value);
        }

        public string AnalyticsUrl
        {
            get => _analyticsUrl;
            set => _analyticsUrl = Uri.UnescapeDataString(value);
        }
        
        public MatchPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = _viewModel = new MatchViewModel(MatchUrl, AnalyticsUrl);
            _viewModel.OnAppearing();
        }
    }
}