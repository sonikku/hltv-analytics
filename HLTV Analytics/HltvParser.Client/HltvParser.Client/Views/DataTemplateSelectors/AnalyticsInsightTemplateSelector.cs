﻿using HltvParser.Client.Models.Analytics;
using Xamarin.Forms;

namespace HltvParser.Client.Views.DataTemplateSelectors
{
    public class AnalyticsInsightTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ProInsightTemplate { get; set; }
        
        public DataTemplate ContraInsightTemplate { get; set; }
        
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return (item as Insight)?.Type == InsightType.Pro ? ProInsightTemplate : ContraInsightTemplate;
        }
    }
}