﻿using HltvParser.Client.Models.Match.Enums;
using HltvParser.Client.Models.Match.Interfaces;
using Xamarin.Forms;

namespace HltvParser.Client.Views.DataTemplateSelectors
{
    public class MatchesListDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate LiveMatchTemplate { get; set; }
        public DataTemplate UpcomingMatchTemplate { get; set; }
        public DataTemplate UndefinedMatchTemplate { get; set; }
        
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            switch (((IMatch) item).MatchType)
            {
                case MatchType.Upcoming:
                    return UpcomingMatchTemplate;
                case MatchType.Live:
                    return LiveMatchTemplate;
                case MatchType.Undefined:
                    return UndefinedMatchTemplate;
                default:
                    return UpcomingMatchTemplate;
            }
        }
    }
}