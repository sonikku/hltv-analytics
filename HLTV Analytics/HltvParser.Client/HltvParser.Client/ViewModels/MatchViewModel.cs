﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HltvParser.Client.Models.Analytics;
using HltvParser.Client.Models.Match;
using HltvParser.Models.Interfaces.Match;
using Xamarin.Forms;

namespace HltvParser.Client.ViewModels
{
    public class MatchViewModel : BaseViewModel
    {
        private UpcomingMatch _match;
        private string _matchUrl;
        private string _analyticsUrl;
        private TimeSpan _timeLeft;
        private bool _alive = true;
        
        private string MatchUrl
        {
            get => _matchUrl;
            set => _matchUrl = "https://hltv.org" + value;
        }

        private string AnalyticsUrl
        {
            get => _analyticsUrl;
            set => _analyticsUrl = "https://hltv.org" + value;
        }

        public UpcomingMatch Match
        {
            get => _match;
            set
            {
                _match = value;
                OnPropertyChanged();
            }
        }

        public TimeSpan TimeLeft
        {
            get => _timeLeft;
            set
            {
                _timeLeft = value;
                OnPropertyChanged();
            }
        }

        public ICommand LoadMatchCommand { get; private set; }
        public ICommand StartTimerCommand { get; private set; }

        public MatchViewModel(string matchUrl, string analyticsUrl)
        {
            MatchUrl = matchUrl;
            AnalyticsUrl = analyticsUrl;
        }

        private async Task LoadMatchAsync()
        {
            Failed = false;
            IsBusy = true;
            try
            {
                Match = await Adapter.GetUpcomingMatch(MatchUrl, AnalyticsUrl);
                TimeLeft = Match.Date - DateTime.Now;
                StartTimerCommand?.Execute(null);
            }
            catch (Exception ex)
            {
                Failed = true;
            }

            IsBusy = false;
        }

        private void StartTimer()
        {
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                if (!(TimeLeft.TotalSeconds > 0) || !_alive) return false;
                TimeLeft = TimeLeft.Subtract(TimeSpan.FromSeconds(1));
                return true;
            });
        }
        
        public void OnAppearing()
        {
            LoadMatchCommand = new Command(async () => await LoadMatchAsync());
            StartTimerCommand = new Command(StartTimer);
            LoadMatchCommand?.Execute(null);
        }
    }
}