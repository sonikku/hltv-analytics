﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Match;
using HltvParser.Client.Views;
using HltvParser.Client.Models.Match.Interfaces;
using Xamarin.Forms;

namespace HltvParser.Client.ViewModels
{
    public class UpcomingMatchesViewModel : BaseViewModel
    {
        private ObservableRangeCollection<IMatch> _matches;

        public ObservableRangeCollection<IMatch> Matches
        {
            get => _matches;
            set
            {
                _matches = value;
                OnPropertyChanged();
            }
        }

        public ICommand LoadMatchesCommand { get; private set; }
        public ICommand GoToMatchCommand => new Command<ShortUpcomingMatch>(async (m) => await GoToMatchAsync(m));

        private async Task LoadMatchesAsync()
        {
            Failed = false;
            IsBusy = true;
            try
            {
                Matches = new ObservableRangeCollection<IMatch>(await Adapter.GetMatchesList());
            }
            catch (Exception ex)
            {
                Failed = true;
            }

            IsBusy = false;
        }

        private async Task GoToMatchAsync(ShortUpcomingMatch match)
        {
            await Shell.Current.GoToAsync(nameof(MatchPage) 
                                          + $"?matchUrl={match.Url}"
                                          + $"&analyticsUrl={match.AnalyticsUrl}");
        }
        
        public void OnAppearing()
        {
            LoadMatchesCommand = new Command(async () => await LoadMatchesAsync());
            LoadMatchesCommand?.Execute(null);
        }
    }
}