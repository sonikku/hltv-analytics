﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using HltvParser.Models.Interfaces.Team;
using Xamarin.Forms;

namespace HltvParser.Client.ViewModels
{
    public class RankingsViewModel : BaseViewModel
    {
        private ObservableCollection<ITeam> _teams  = new ObservableCollection<ITeam>();

        public ObservableCollection<ITeam> Teams
        {
            get => _teams;
            set
            {
                _teams = value;
                OnPropertyChanged();
            }
        }

        public ICommand LoadRankingsCommand { get; private set; }

        private async Task LoadRankingsAsync()
        {
            Failed = false;
            IsBusy = true;
            try
            {
                var rankings = await Facade.GetRankings();
                Teams = new ObservableCollection<ITeam>(rankings);
            }
            catch (Exception ex)
            {
                Failed = true;
            }
            IsBusy = false;
        }

        public void OnAppearing()
        {
            LoadRankingsCommand = new Command(async () => await LoadRankingsAsync());
            LoadRankingsCommand?.Execute(null);
        }
    }
}