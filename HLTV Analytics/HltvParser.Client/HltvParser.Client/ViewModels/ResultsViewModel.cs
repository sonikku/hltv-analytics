﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Match;
using Xamarin.Forms;

namespace HltvParser.Client.ViewModels
{
    public class ResultsViewModel : BaseViewModel
    {
        private ObservableRangeCollection<EndedMatch> _results;

        public ObservableRangeCollection<EndedMatch> Results
        {
            get => _results;
            set
            {
                _results = value;
                OnPropertyChanged();
            }
        }

        public ICommand LoadResultsCommand { get; private set; } 

        private async Task LoadResultsAsync()
        {
            Failed = false;
            IsBusy = true;
            try
            {
                Results = new ObservableRangeCollection<EndedMatch>(await Adapter.GetResults());
            }
            catch (Exception ex)
            {
                Failed = true;
            }

            IsBusy = false;
        }
        
        public void OnAppearing()
        {
            LoadResultsCommand = new Command(async () => await LoadResultsAsync());
            LoadResultsCommand?.Execute(null);
        }
    }
}