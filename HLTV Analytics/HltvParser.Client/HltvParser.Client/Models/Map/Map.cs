﻿using HltvParser.Client.Models.Map.Interfaces;

namespace HltvParser.Client.Models.Map
{
    public class Map : IMap
    {
        public string Name { get; set; }
    }
}