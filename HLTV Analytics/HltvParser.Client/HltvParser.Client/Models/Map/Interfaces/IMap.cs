﻿namespace HltvParser.Client.Models.Map.Interfaces
{
    public interface IMap
    {
        string Name { get; set; }
    }
}