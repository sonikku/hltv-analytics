﻿namespace HltvParser.Client.Models.Score.Interfaces
{
    public interface IScoreKept
    {
        IScore Scores { get; set; }
    }
}