﻿namespace HltvParser.Client.Models.Score.Interfaces
{
    public interface IScore
    {
        short HomeScore { get; set; }
        short AwayScore { get; set; }
    }
}