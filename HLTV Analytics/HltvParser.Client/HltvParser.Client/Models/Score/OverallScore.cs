﻿using HltvParser.Client.Models.Score.Interfaces;

namespace HltvParser.Client.Models.Score
{
    public class OverallScore : IScore
    {
        public short HomeScore { get; set; }
        public short AwayScore { get; set; }

        public OverallScore(HltvParser.Models.Interfaces.Score.IScore score)
        {
            HomeScore = score.HomeScore;
            AwayScore = score.AwayScore;
        }
    }
}