﻿using HltvParser.Client.Models.Common.Interfaces;

namespace HltvParser.Client.Models.Common
{
    public class Country : ICountry
    {
        public string ImageUrl { get; set; }
        public string Name { get; set; }

        public Country(HltvParser.Models.Country country)
        {
            Name = country.Name;
            ImageUrl = country.ImageUrl;
        }
    }
}