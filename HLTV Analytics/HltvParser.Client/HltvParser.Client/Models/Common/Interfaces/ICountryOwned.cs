﻿namespace HltvParser.Client.Models.Common.Interfaces
{
    public interface ICountryOwned
    {
        ICountry Country { get; set; }
    }
}