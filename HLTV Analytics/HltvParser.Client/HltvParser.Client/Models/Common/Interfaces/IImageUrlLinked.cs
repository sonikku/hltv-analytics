﻿namespace HltvParser.Client.Models.Common.Interfaces
{
    public interface IImageUrlLinked
    {
        string ImageUrl { get; set; }
    }
}