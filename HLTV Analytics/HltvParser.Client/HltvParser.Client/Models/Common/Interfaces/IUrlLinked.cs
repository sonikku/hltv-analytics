﻿namespace HltvParser.Client.Models.Common.Interfaces
{
    public interface IUrlLinked
    {
        string Url { get; set; }
    }
}