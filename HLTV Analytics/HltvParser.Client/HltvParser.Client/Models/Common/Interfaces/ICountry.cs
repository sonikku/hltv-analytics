﻿namespace HltvParser.Client.Models.Common.Interfaces
{
    public interface ICountry : IImageUrlLinked
    {
        string Name { get; set; }
    }
}