﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Analytics;
using HltvParser.Client.Models.Common;
using HltvParser.Client.Models.Match.Enums;
using HltvParser.Client.Models.Match.Interfaces;
using HltvParser.Client.Models.Player.Interfaces;
using HltvParser.Client.Models.Score;
using HltvParser.Client.Models.Score.Interfaces;
using HltvParser.Client.Models.Team;
using HltvParser.Models.Interfaces.Analytics;

namespace HltvParser.Client.Models.Match
{
    public class LiveMatch : IMatch<MatchTeam>, IScoreKept
    {
        public MatchType MatchType { get; set; }
        public MatchTeam HomeTeam { get; set; }
        public MatchTeam AwayTeam { get; set; }
        public DateTime Date { get; set; }
        public IScore Scores { get; set; }

        public LiveMatch(HltvParser.Models.Match.LiveMatch match, IAnalytics analytics)
        {
            HomeTeam = new MatchTeam()
            {
                ImageUrl = ((HltvParser.Models.Team.MatchTeam)(match.HomeTeam)).ImageUrl,
                Name = match.HomeTeam.Name,
                Analytics = new TeamAnalytics(analytics.HomeTeamAnalytics),
                Country = new Country(((HltvParser.Models.Team.MatchTeam)match.HomeTeam).Country),
                Players = new ObservableRangeCollection<IPlayer>(
                    ((HltvParser.Models.Team.MatchTeam)(match.HomeTeam))
                    .Players
                    .Select(i => new Player.Player()
                    {
                        Nickname = i.Nickname
                    }))
            };
            AwayTeam = new MatchTeam()
            {
                ImageUrl = ((HltvParser.Models.Team.MatchTeam)(match.HomeTeam)).ImageUrl,
                Name = match.AwayTeam.Name,
                Analytics = new TeamAnalytics(analytics.AwayTeamAnalytics),
                Country = new Country(((HltvParser.Models.Team.MatchTeam)match.AwayTeam).Country),
                Players = new ObservableRangeCollection<IPlayer>(
                    ((HltvParser.Models.Team.MatchTeam)(match.AwayTeam))
                    .Players
                    .Select(i => new Player.Player()
                    {
                        Nickname = i.Nickname
                    }))
            };
            Date = match.Date;
            Scores = new OverallScore(match.Scores);
        }
    }
}