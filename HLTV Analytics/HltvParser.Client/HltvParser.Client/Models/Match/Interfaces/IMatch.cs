﻿using System;
using HltvParser.Client.Models.Match.Enums;
using HltvParser.Client.Models.Team.Interfaces;

namespace HltvParser.Client.Models.Match.Interfaces
{
    public interface IMatch
    {
        MatchType MatchType { get; set; } 
        DateTime Date { get; set; }
    }
    
    public interface IMatch<TTeam> : IMatch where TTeam : ITeam
    {
        TTeam HomeTeam { get; set; }
        TTeam AwayTeam { get; set; }
    }
}