﻿using System.Collections.Generic;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Map.Interfaces;

namespace HltvParser.Client.Models.Match.Interfaces
{
    public interface IMapped
    {
        ObservableRangeCollection<IMap> Maps { get; set; }
    }
}