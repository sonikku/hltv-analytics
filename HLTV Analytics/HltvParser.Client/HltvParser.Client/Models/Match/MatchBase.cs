﻿using System;
using HltvParser.Client.Models.Match.Enums;
using HltvParser.Client.Models.Match.Interfaces;

namespace HltvParser.Client.Models.Match
{
    public class MatchBase : IMatch
    {
        public MatchType MatchType { get; set; } = MatchType.Undefined;
        public DateTime Date { get; set; }
        public string Comment { get; set; }

        public MatchBase(HltvParser.Models.Interfaces.Match.IMatch match)
        {
            Date = match.Date;
            Comment = match.Comment;
        }
    }
}