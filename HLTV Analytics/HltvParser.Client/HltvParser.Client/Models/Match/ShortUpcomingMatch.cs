﻿using System;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Map.Interfaces;
using HltvParser.Client.Models.Match.Enums;
using HltvParser.Client.Models.Match.Interfaces;
using HltvParser.Client.Models.Team;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Team;

namespace HltvParser.Client.Models.Match
{
    public class ShortUpcomingMatch : IMatch<ShortTeam>, IUrlLinked, IAnalyticsLinked
    {
        public MatchType MatchType { get; set; } = MatchType.Upcoming;
        public DateTime Date { get; set; }
        public ShortTeam HomeTeam { get; set; }
        public ShortTeam AwayTeam { get; set; }
        public string Url { get; set; }
        public string AnalyticsUrl { get; set; }

        public ShortUpcomingMatch(HltvParser.Models.Match.UpcomingMatch match)
        {
            Date = match.Date;
            HomeTeam = new ShortTeam()
            {
                ImageUrl = ((TeamBase) (match.HomeTeam)).ImageUrl,
                Name = match.HomeTeam.Name
            };
            AwayTeam = new ShortTeam()
            {
                ImageUrl = ((TeamBase) (match.AwayTeam)).ImageUrl,
                Name = match.AwayTeam.Name
            };
            Url = match.Url;
            AnalyticsUrl = match.AnalyticsUrl;
        }
    }
}