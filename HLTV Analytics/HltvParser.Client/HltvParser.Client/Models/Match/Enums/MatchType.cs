﻿namespace HltvParser.Client.Models.Match.Enums
{
    public enum MatchType
    {
        Live,
        Upcoming,
        Undefined,
        Ended
    }
}