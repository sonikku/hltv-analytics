﻿using System;
using HltvParser.Client.Models.Match.Enums;
using HltvParser.Client.Models.Match.Interfaces;
using HltvParser.Client.Models.Score;
using HltvParser.Client.Models.Score.Interfaces;
using HltvParser.Client.Models.Team;
using HltvParser.Models.Team;

namespace HltvParser.Client.Models.Match
{
    public class ShortLiveMatch : IMatch<ShortTeam>, IScoreKept
    {
        public MatchType MatchType { get; set; }
        public DateTime Date { get; set; }
        public ShortTeam HomeTeam { get; set; }
        public ShortTeam AwayTeam { get; set; }
        public IScore Scores { get; set; }
        
        public ShortLiveMatch(HltvParser.Models.Match.LiveMatch match)
        {
            Date = match.Date;
            HomeTeam = new ShortTeam()
            {
                ImageUrl = ((TeamBase) (match.HomeTeam)).ImageUrl,
                Name = match.HomeTeam.Name
            };
            AwayTeam = new ShortTeam()
            {
                ImageUrl = ((TeamBase) (match.AwayTeam)).ImageUrl,
                Name = match.AwayTeam.Name
            };
            Scores = new OverallScore(match.Scores);
        }
    }
}