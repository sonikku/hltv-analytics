﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Analytics;
using HltvParser.Client.Models.Common;
using HltvParser.Client.Models.Map.Interfaces;
using HltvParser.Client.Models.Match.Interfaces;
using HltvParser.Client.Models.Player.Interfaces;
using HltvParser.Client.Models.Team;
using HltvParser.Models.Interfaces.Analytics;
using HltvParser.Models.Player;
using MatchType = HltvParser.Client.Models.Match.Enums.MatchType;

namespace HltvParser.Client.Models.Match
{
    public class UpcomingMatch : IMatch<MatchTeam>, IMapped
    {
        public MatchType MatchType { get; set; } = MatchType.Upcoming;
        public MatchTeam HomeTeam { get; set; }
        public MatchTeam AwayTeam { get; set; }
        public DateTime Date { get; set; }
        public ObservableRangeCollection<IMap> Maps { get; set; }

        public UpcomingMatch(HltvParser.Models.Match.UpcomingMatch match, IAnalytics analytics)
        {
            HomeTeam = new MatchTeam()
            {
                ImageUrl = ((HltvParser.Models.Team.MatchTeam)(match.HomeTeam)).ImageUrl,
                Name = match.HomeTeam.Name,
                Analytics = new TeamAnalytics(analytics.HomeTeamAnalytics),
                Country = new Country(((HltvParser.Models.Team.MatchTeam)match.HomeTeam).Country),
                Players = new ObservableCollection<IPlayer>(
                    ((HltvParser.Models.Team.MatchTeam)(match.HomeTeam))
                    .Players
                    .Select(i => new Player.Player()
                    {
                        Nickname = i.Nickname
                    }))
            };
            AwayTeam = new MatchTeam()
            {
                ImageUrl = ((HltvParser.Models.Team.MatchTeam)(match.HomeTeam)).ImageUrl,
                Name = match.AwayTeam.Name,
                Analytics = new TeamAnalytics(analytics.AwayTeamAnalytics),
                Country = new Country(((HltvParser.Models.Team.MatchTeam)match.AwayTeam).Country),
                Players = new ObservableCollection<IPlayer>(
                    ((HltvParser.Models.Team.MatchTeam)(match.AwayTeam))
                    .Players
                    .Select(i => new Player.Player()
                    {
                        Nickname = i.Nickname
                    }))
            };
            Date = match.Date;
            Maps = new ObservableRangeCollection<IMap>(match.Maps.Select(k => new Map.Map()
            {
                Name = k.Name
            }));
        }
    }
}