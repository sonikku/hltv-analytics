﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Common;
using HltvParser.Client.Models.Match.Enums;
using HltvParser.Client.Models.Match.Interfaces;
using HltvParser.Client.Models.Player.Interfaces;
using HltvParser.Client.Models.Score.Interfaces;
using HltvParser.Client.Models.Team.Interfaces;
using HltvParser.Client.Models.Score;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Team;
using MatchTeam = HltvParser.Client.Models.Team.MatchTeam;

namespace HltvParser.Client.Models.Match
{
    public class EndedMatch : IMatch<MatchTeam>, IScoreKept
    {
        public MatchType MatchType { get; set; } = MatchType.Ended;
        public MatchTeam HomeTeam { get; set; }
        public MatchTeam AwayTeam { get; set; }
        public DateTime Date { get; set; }
        public IScore Scores { get; set; }
        
        public EndedMatch(HltvParser.Models.Match.Match match)
        {
            HomeTeam = new MatchTeam()
            {
                ImageUrl = ((IImageLinked)match.HomeTeam).ImageUrl,
                Name = match.HomeTeam.Name,
                //Country = new Country(((INaturalized)match.HomeTeam).Country),
                // Players = new ObservableRangeCollection<IPlayer>(
                //     ((HltvParser.Models.Team.MatchTeam)(match.HomeTeam))
                //     .Players
                //     .Select(i => new Player.Player()
                //     {
                //         Nickname = i.Nickname
                //     }))
            };
            AwayTeam = new MatchTeam()
            {
                ImageUrl = ((IImageLinked)(match.HomeTeam)).ImageUrl,
                Name = match.AwayTeam.Name,
                //Country = new Country(((HltvParser.Models.Team.MatchTeam)match.AwayTeam).Country),
                // Players = new ObservableCollection<IPlayer>(
                //     ((HltvParser.Models.Team.MatchTeam)(match.AwayTeam))
                //     .Players
                //     .Select(i => new Player.Player()
                //     {
                //         Nickname = i.Nickname
                //     }))
            };
            Date = match.Date;
            Scores = new OverallScore(match.Scores);
        }
    }
}