﻿using HltvParser.Client.Models.Player.Interfaces;

namespace HltvParser.Client.Models.Player
{
    public class Player : IPlayer
    {
        public int PlayerId { get; set; }
        public string FullName { get; set; }
        public string Nickname { get; set; }
    }
}