﻿namespace HltvParser.Client.Models.Player.Interfaces
{
    public interface IPlayer
    {
        string FullName { get; set; }
        string Nickname { get; set; }
        int PlayerId { get; set; }
    }
}