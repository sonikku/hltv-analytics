﻿using HltvParser.Client.Models.Analytics.Interfaces;

namespace HltvParser.Client.Models.Analytics
{
    public class Insight : IInsight
    {
        public string Comment { get; set; }
        public InsightType Type { get; set; }
    }
}