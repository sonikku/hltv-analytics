﻿using System.Linq;
using HltvParser.Client.DataStructures;
using HltvParser.Client.Models.Analytics.Interfaces;
using HltvParser.Models.Interfaces.Analytics;

namespace HltvParser.Client.Models.Analytics
{
    public class TeamAnalytics : ObservableRangeCollection<IInsight>
    {
        public TeamAnalytics(ITeamAnalytics analytics)
        {
            AddRange(analytics.SummaryPro.Select(i => new Insight()
            {
                Comment = i,
                Type = InsightType.Pro
            }));
            AddRange(analytics.SummaryContra.Select(i => new Insight()
            {
                Comment = i,
                Type = InsightType.Contra
            }));
        }
    }
}