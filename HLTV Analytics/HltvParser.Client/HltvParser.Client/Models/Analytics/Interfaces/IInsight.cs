﻿namespace HltvParser.Client.Models.Analytics.Interfaces
{
    public interface IInsight
    {
        string Comment { get; set; }
        InsightType Type { get; set; }
    }
}