﻿namespace HltvParser.Client.Models.Analytics.Interfaces
{
    public interface IAnalytics
    {
        TeamAnalytics HomeTeamAnalytics { get; set; }
        TeamAnalytics AwayTeamAnalytics { get; set; }
    }
}