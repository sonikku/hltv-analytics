﻿using HltvParser.Client.Models.Common.Interfaces;
using HltvParser.Client.Models.Team.Interfaces;

namespace HltvParser.Client.Models.Team
{
    public class ShortTeam : ITeam, IImageUrlLinked
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}