﻿namespace HltvParser.Client.Models.Team.Interfaces
{
    public interface ITeam
    {
        string Name { get; set; }
    }
}