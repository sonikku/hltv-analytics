﻿namespace HltvParser.Client.Models.Team.Interfaces
{
    public interface IRanked
    {
        int WorldRank { get; set; }
        int Points { get; set; }
    }
}