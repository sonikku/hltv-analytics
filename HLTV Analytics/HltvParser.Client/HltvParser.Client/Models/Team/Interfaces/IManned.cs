﻿using System.Collections.Generic;
using HltvParser.Client.Models.Player.Interfaces;

namespace HltvParser.Client.Models.Team.Interfaces
{
    public interface IManned
    {
        ICollection<IPlayer> Players { get; set; }
    }
}