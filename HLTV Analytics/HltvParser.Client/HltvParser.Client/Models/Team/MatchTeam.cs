﻿using System.Collections.ObjectModel;
using HltvParser.Client.Models.Analytics;
using HltvParser.Client.Models.Common.Interfaces;
using HltvParser.Client.Models.Player.Interfaces;
using HltvParser.Client.Models.Team.Interfaces;

namespace HltvParser.Client.Models.Team
{
    public class MatchTeam : ITeam, IImageUrlLinked, ICountryOwned
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public TeamAnalytics Analytics { get; set; }
        public ICountry Country { get; set; }
        public ObservableCollection<IPlayer> Players { get; set; }
    }
}