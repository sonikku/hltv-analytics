namespace HltvParser.Models.Interfaces
{
    public interface IMatchLinked
    {
        string MatchUrl { get; set; }
    }
}