namespace HltvParser.Models.Interfaces
{
    public interface IImageLinked
    {
        string ImageUrl { get; set; }
    }
}