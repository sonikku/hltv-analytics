namespace HltvParser.Models.Interfaces.Player
{
    public interface INamedPlayer
    {
        string FullName { get; set; }
    }
}