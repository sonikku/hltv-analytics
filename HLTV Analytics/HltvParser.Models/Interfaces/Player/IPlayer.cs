namespace HltvParser.Models.Interfaces.Player
{
    public interface IPlayer
    {
        string Nickname { get; set; }
        
        uint PlayerId { get; set; }
    }
}