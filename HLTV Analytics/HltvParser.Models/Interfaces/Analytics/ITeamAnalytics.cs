using System.Collections.Generic;
using HltvParser.Models.Interfaces.Team;

namespace HltvParser.Models.Interfaces.Analytics
{
    public interface ITeamAnalytics
    {
        IEnumerable<string> SummaryPro { get; set; }
        IEnumerable<string> SummaryContra { get; set; }
    }
}