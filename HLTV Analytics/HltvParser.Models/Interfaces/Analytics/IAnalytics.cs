namespace HltvParser.Models.Interfaces.Analytics
{
    public interface IAnalytics
    {
        ITeamAnalytics HomeTeamAnalytics { get; set; }
        ITeamAnalytics AwayTeamAnalytics { get; set; }
    }
}