namespace HltvParser.Models.Interfaces.Analytics
{
    public interface IHandicap
    {
        float Wins2To0 { get; set; }
        float Wins2To1 { get; set; }
        float Losses0To2 { get; set; }
        float Losses1To2 { get; set; }
        float Overtimes { get; set; }
        float AverageRoundsLostInWins { get; set; }
        float AverageRoundsWonInLosses { get; set; }
    }
}