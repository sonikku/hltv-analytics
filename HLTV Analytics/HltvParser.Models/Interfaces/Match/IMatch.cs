using System;
using HltvParser.Models.Enums;
using HltvParser.Models.Interfaces.Team;
using HltvParser.Models.Team;

namespace HltvParser.Models.Interfaces.Match
{
    public interface IMatch
    {
        MatchType MatchType { get; }
        DateTime Date { get; set; }
        ITeam HomeTeam { get; set; }
        ITeam AwayTeam { get; set; }
        string Comment { get; set; }
    }
}