﻿using System.Collections.Generic;

namespace HltvParser.Models.Interfaces.Match
{
    public interface IMapped
    {
        IEnumerable<IMap> Maps { get; set; }
    }
}