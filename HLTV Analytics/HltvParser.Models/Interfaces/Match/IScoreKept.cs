using System.Collections.Generic;
using HltvParser.Models.Interfaces.Score;

namespace HltvParser.Models.Interfaces.Match
{
    public interface IScoreKept
    {
        IScore Scores { get; set; }
    }
}