namespace HltvParser.Models.Interfaces.Score
{
    public interface IScore
    {
        short HomeScore { get; set; }
        short AwayScore { get; set; }
    }
}