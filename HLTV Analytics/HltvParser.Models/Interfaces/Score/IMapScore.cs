﻿namespace HltvParser.Models.Interfaces.Score
{
    public interface IMapScore
    {
        IMap Map { get; set; }
    }
}