﻿using System.Collections.Generic;
using HltvParser.Models.Score;

namespace HltvParser.Models.Interfaces.Score
{
    public interface IMatchScore
    {
        IEnumerable<MapScore> MapScores { get; set; }
    }
}