namespace HltvParser.Models.Interfaces
{
    public interface IUrlLinked
    {
        string Url { get; set; }
    }
}