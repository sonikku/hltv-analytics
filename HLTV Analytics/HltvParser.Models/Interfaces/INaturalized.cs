﻿namespace HltvParser.Models.Interfaces
{
    public interface INaturalized
    {
        Country Country { get; set; }
    }
}