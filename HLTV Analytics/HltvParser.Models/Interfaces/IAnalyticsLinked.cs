namespace HltvParser.Models.Interfaces
{
    public interface IAnalyticsLinked
    {
        string AnalyticsUrl { get; set; }
    }
}