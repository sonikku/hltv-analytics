﻿namespace HltvParser.Models.Interfaces.Team
{
    public interface IRanked
    {
        int WorldRank { get; set; }
        int Points { get; set; }
    }
}