namespace HltvParser.Models.Interfaces.Team
{
    public interface ITeam
    {
        string Name { get; set; }
    }
}