using System.Collections.Generic;
using HltvParser.Models.Interfaces.Player;

namespace HltvParser.Models.Interfaces.Team
{
    public interface IManned
    {
        IEnumerable<IPlayer> Players { get; set; }
    }
}