using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Score;
using HltvParser.Models.Match;

namespace HltvParser.Models.Score
{
    public class MapScore : ScoreBase, IMapScore
    {
        public IMap Map { get; set; }
    }
}