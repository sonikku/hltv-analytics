﻿using System.Collections.Generic;
using System.Linq;
using HltvParser.Models.Interfaces.Score;

namespace HltvParser.Models.Score
{
    public class OverallScore : ScoreBase, IMatchScore
    {
        public IEnumerable<MapScore> MapScores { get; set; }

        public override short HomeScore => (short)MapScores.Count(i => i.HomeScore > AwayScore);
        
        public override short AwayScore => (short)MapScores.Count(i => i.HomeScore < AwayScore);
    }
}