using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Score;

namespace HltvParser.Models.Score
{
    public class ScoreBase : IScore
    {
        public virtual short HomeScore { get; set; }
        public virtual short AwayScore { get; set; }
    }
}