﻿using HltvParser.Models.Interfaces;

namespace HltvParser.Models
{
    public class Country : IImageLinked
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}