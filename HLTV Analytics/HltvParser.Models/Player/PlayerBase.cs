using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Player;

namespace HltvParser.Models.Player
{
    public class PlayerBase : IPlayer, IUrlLinked
    {
        public string Nickname { get; set; }
        public uint PlayerId { get; set; }
        public string Url { get; set; }
    }
}