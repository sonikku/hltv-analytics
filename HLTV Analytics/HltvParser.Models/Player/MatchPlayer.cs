using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Player;

namespace HltvParser.Models.Player
{
    public class MatchPlayer : PlayerBase, INamedPlayer, IImageLinked
    {
        public string ImageUrl { get; set; }
        
        public string FullName { get; set; }
    }
}