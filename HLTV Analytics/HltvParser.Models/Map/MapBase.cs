using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Team;

namespace HltvParser.Models.Map
{
    public class MapBase : IMap
    {
        public string Name { get; set; }
    }
}