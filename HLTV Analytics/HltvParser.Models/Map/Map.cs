using HltvParser.Models.Interfaces;

namespace HltvParser.Models.Map
{
    public class Map : MapBase, IImageLinked
    {
        public string ImageUrl { get; set; }
    }
}