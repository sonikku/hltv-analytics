using System.Collections.Generic;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Analytics;
using HltvParser.Models.Interfaces.Team;

namespace HltvParser.Models.Analytics
{
    public class TeamAnalytics : ITeamAnalytics, ITeam, IImageLinked
    {
        public IEnumerable<string> SummaryPro { get; set; }
        public IEnumerable<string> SummaryContra { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}