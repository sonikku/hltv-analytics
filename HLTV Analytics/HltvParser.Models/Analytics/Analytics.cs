using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Analytics;

namespace HltvParser.Models.Analytics
{
    public class Analytics : IAnalytics, IMatchLinked, IUrlLinked
    {
        public ITeamAnalytics HomeTeamAnalytics { get; set; } = new TeamAnalytics();
        public ITeamAnalytics AwayTeamAnalytics { get; set; } = new TeamAnalytics();
        public string MatchUrl { get; set; }
        public string Url { get; set; }
    }
}