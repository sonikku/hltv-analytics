using HltvParser.Models.Interfaces.Analytics;

namespace HltvParser.Models.Analytics
{
    public class Handicap : IHandicap
    {
        public float Wins2To0 { get; set; }
        public float Wins2To1 { get; set; }
        public float Losses0To2 { get; set; }
        public float Losses1To2 { get; set; }
        public float Overtimes { get; set; }
        public float AverageRoundsLostInWins { get; set; }
        public float AverageRoundsWonInLosses { get; set; }
    }
}