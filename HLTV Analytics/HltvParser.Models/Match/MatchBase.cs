using System;
using HltvParser.Models.Enums;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Match;
using HltvParser.Models.Interfaces.Team;
using HltvParser.Models.Team;

namespace HltvParser.Models.Match
{
    public class MatchBase : IMatch, IUrlLinked
    {
        public virtual MatchType MatchType => MatchType.Undefined;

        public DateTime Date { get; set; } = new DateTime();
        public ITeam HomeTeam { get; set; } = new MatchTeam();
        public ITeam AwayTeam { get; set; } = new MatchTeam();
        public string Url { get; set; } = "";
        public string Comment { get; set; } = "";
        
        public MatchBase() {}

        public MatchBase(MatchBase match)
        {
            Date = match.Date;
            Url = match.Url;
            AwayTeam = match.AwayTeam;
            HomeTeam = match.HomeTeam;
        }
    }
}