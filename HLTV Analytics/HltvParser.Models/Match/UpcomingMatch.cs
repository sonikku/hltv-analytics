using System.Collections.Generic;
using HltvParser.Models.Enums;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Match;

namespace HltvParser.Models.Match
{
    public class UpcomingMatch : MatchBase, IAnalyticsLinked, IMapped
    {
        public override MatchType MatchType => MatchType.Upcoming;
        
        public string AnalyticsUrl { get; set; }

        public UpcomingMatch() {}
        
        public UpcomingMatch(MatchBase match) : base(match) {}
        public IEnumerable<IMap> Maps { get; set; }
    }
}