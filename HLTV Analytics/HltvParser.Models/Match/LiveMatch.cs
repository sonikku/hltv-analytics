using System.Collections.Generic;
using HltvParser.Models.Enums;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Match;
using HltvParser.Models.Interfaces.Score;

namespace HltvParser.Models.Match
{
    public class LiveMatch : MatchBase, IAnalyticsLinked, IScoreKept
    {
        public override MatchType MatchType => MatchType.Live;
        
        public string AnalyticsUrl { get; set; }
        public IScore Scores { get; set; }
        
        public LiveMatch() {}
        
        public LiveMatch(MatchBase match) : base(match) {}
    }
}