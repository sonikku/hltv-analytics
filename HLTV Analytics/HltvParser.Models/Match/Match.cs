using System.Collections.Generic;
using HltvParser.Models.Enums;
using HltvParser.Models.Interfaces.Match;
using HltvParser.Models.Interfaces.Score;

namespace HltvParser.Models.Match
{
    public class Match : MatchBase, IScoreKept
    {
        public override MatchType MatchType => MatchType.Ended;
        
        public IScore Scores { get; set; }
        
        public Match(MatchBase match) : base(match) {}
        
        public Match() {}
    }
}