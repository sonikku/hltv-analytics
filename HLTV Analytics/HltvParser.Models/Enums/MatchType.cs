namespace HltvParser.Models.Enums
{
    public enum MatchType
    {
        Upcoming,
        Live,
        Ended,
        Undefined
    }
}