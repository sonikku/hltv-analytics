namespace HltvParser.Models.Enums
{
    public enum ContextStatus
    {
        NotReady,
        Ready,
        Loaded
    }
}