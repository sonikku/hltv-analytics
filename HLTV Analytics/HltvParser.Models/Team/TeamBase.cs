using System.Collections.Generic;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Team;
using HltvParser.Models.Player;

namespace HltvParser.Models.Team
{
    public class TeamBase : ITeam, IImageLinked
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}