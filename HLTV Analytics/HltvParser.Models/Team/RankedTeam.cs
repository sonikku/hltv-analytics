﻿using System.Collections.Generic;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Player;
using HltvParser.Models.Interfaces.Team;

namespace HltvParser.Models.Team
{
    public class RankedTeam : ITeam, IManned, IRanked, IImageLinked
    {
        public string Name { get; set; }
        public IEnumerable<IPlayer> Players { get; set; }
        public int WorldRank { get; set; }
        public int Points { get; set; }
        public string ImageUrl { get; set; }
    }
}