using System.Collections.Generic;
using HltvParser.Models.Interfaces;
using HltvParser.Models.Interfaces.Player;
using HltvParser.Models.Interfaces.Team;

namespace HltvParser.Models.Team
{
    public class MatchTeam : TeamBase, IManned, IUrlLinked, INaturalized
    {
        public IEnumerable<IPlayer> Players { get; set; }
        public string Url { get; set; }
        
        public Country Country { get; set; }
    }
}