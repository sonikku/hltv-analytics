using System.Collections.Generic;
using System.Linq;
using AngleSharp.Dom;

namespace HltvParser.Core.Bridges.AngleSharp.Extensions
{
    internal static class ElementExtensions
    {
        internal static Element Convert(this IElement element)
        {
            return new Element(element);
        }

        internal static IEnumerable<Element> Convert(this IEnumerable<IElement> elements)
        {
            return elements == null ? 
                new List<Element>() : 
                elements.Select(e => e.Convert()).ToList();
        }
    }
}