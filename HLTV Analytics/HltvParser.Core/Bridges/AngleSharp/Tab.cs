using System;
using System.Net;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Io;
using HltvParser.Core.Base;
using HltvParser.Core.Exceptions;

namespace HltvParser.Core.Bridges.AngleSharp
{
    public class Tab : TabBase<IBrowsingContext, IDocument, IElement>
    {
        public Tab()
        {
            InitBrowsingContext();
        }
        
        /// <summary>
        /// Инициализирует вкладку AngleSharp'а
        /// </summary>
        private void InitBrowsingContext()
        {
            var parserConfig = Configuration.Default.WithDefaultLoader(new LoaderOptions()
            {
                IsNavigationDisabled = !(Config.AllowNavigation),
                IsResourceLoadingEnabled = true
            });
            if (Config.UseCookies) parserConfig.WithDefaultCookies();
            if (Config.UseJavaScript) parserConfig.WithJs();
            InnerTab = BrowsingContext.New(parserConfig);
        }

        /// <summary>
        /// Перейти по указанному адресу и загрузить страницу
        /// </summary>
        /// <param name="address">Адрес веб-страницы</param>
        /// <exception cref="ArgumentNullException">Выбрасывается при попытке
        /// загрузить страницу с пустым адресом.</exception>
        /// <exception cref="LoadingDocumentException">Выбрасывается при неудачной
        /// загрузке документа</exception>
        /// <returns></returns>
        public async Task LoadDocumentAsync(string address)
        {
            if (!string.IsNullOrEmpty(address))
                Address = address;
            else
                throw new ArgumentNullException(nameof(address));
            var doc = await InnerTab.OpenAsync(Address).WhenStable();
            if (doc.StatusCode != HttpStatusCode.OK)
                throw new LoadingDocumentException(Address,"HTTP status is not OK.");
            Document = new Document(doc);
        }
    }
}