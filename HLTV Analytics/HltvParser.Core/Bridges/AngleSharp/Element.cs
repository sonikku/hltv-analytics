﻿using System.Collections.Generic;
using System.Linq;
using AngleSharp.Common;
using HltvParser.Core.Base;
using HltvParser.Core.Bridges.AngleSharp.Extensions;
using HltvParser.Core.Exceptions;
using HltvParser.Core.Interfaces;
using IAngleSharpElement = AngleSharp.Dom.IElement;

namespace HltvParser.Core.Bridges.AngleSharp
{
    public class Element : ElementBase<IAngleSharpElement>
    {
        public override string Name => InnerElement.TagName;

        public override string Id
        {
            get => InnerElement.Id;
            set => InnerElement.Id = value;
        }

        public override string Class
        {
            get => InnerElement.ClassName;
            set => InnerElement.ClassName = value;
        }

        public override string Content
        {
            get => InnerElement.InnerHtml;
            set => InnerElement.InnerHtml = value;
        }

        public override string TextContent
        {
            get => InnerElement.TextContent;
            set => InnerElement.TextContent = value;
        }

        public override IDictionary<string,string> Attributes => InnerElement
            .Attributes
            .ToDictionary(i => i.Name, k => k.Value);

        public override IEnumerable<IElement> Children => InnerElement.Children.Convert();

        public override IEnumerable<string> Classes => InnerElement.ClassList.ToList();

        public override IEnumerable<IElement> GetElementsByClassName(string className)
        {
            var elements = InnerElement
                .GetElementsByClassName(className);
            if (elements == null)
                return new List<Element>();
            return elements
                .Select(element => element.Convert())
                .ToList();
        }

        public override IEnumerable<IElement> GetElementsByTagName(string tagName)
        {
            var elements = InnerElement
                .GetElementsByTagName(tagName);
            if (elements == null)
                return new List<Element>();
            return elements
                .Select(element => element.Convert())
                .ToList();
        }

        public override string GetAttributeValue(string attribute)
        {
            var found = Attributes.TryGetValue(attribute, out var value);
            if (found)
                return value;
            throw new AttributeNotFoundException(attribute);
        }

        public override bool TryGetAttribute(string attribute, out string value)
        {
            return Attributes.TryGetValue(attribute, out value);
        }

        public Element(IAngleSharpElement element) : base(element)
        {
            InnerElement = element ?? 
                           throw new InnerElementNullException("Inner element is null");
        }
    }
}