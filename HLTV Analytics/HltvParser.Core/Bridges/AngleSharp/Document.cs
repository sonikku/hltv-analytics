using System.Collections.Generic;
using AngleSharp.Dom;
using HltvParser.Core.Base;
using HltvParser.Core.Bridges.AngleSharp.Extensions;

namespace HltvParser.Core.Bridges.AngleSharp
{
    public class Document : DocumentBase<IDocument, IElement>
    {
        public override IEnumerable<ElementBase<IElement>> Elements => InnerDocument.Children.Convert();

        public override string Title => InnerDocument.Title;

        public override Interfaces.IElement GetElementById(string id)
        {
            return new Element(InnerDocument.GetElementById(id));
        }

        public override IEnumerable<Interfaces.IElement> GetElementsByClassName(string className)
        {
            return InnerDocument.GetElementsByClassName(className).Convert();
        }

        public override IEnumerable<Interfaces.IElement> GetElementsByTagName(string tagName)
        {
            return InnerDocument.GetElementsByTagName(tagName).Convert();
        }

        public Document(IDocument document) : base(document)
        {
        }
    }
}