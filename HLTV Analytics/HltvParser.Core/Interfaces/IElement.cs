using System.Collections.Generic;
using HltvParser.Core.Base;

namespace HltvParser.Core.Interfaces
{
    public interface IElement
    {
        /// <summary>
        /// Название элемента (тег). Например, body, img, a, p, h1 и так далее.
        /// </summary>
        string Name { get; }
        
        /// <summary>
        /// ID элемента (если есть).
        /// Стандарт HTML требует уникальности ID элемента, но данное условие не
        /// выполняется гарантированно.
        /// </summary>
        string Id { get; set; }
        
        /// <summary>
        /// Название класса элемента (если есть). Если имеется несколько
        /// классов, то они все будут перечислены через пробел.
        /// </summary>
        string Class { get; set; }
        
        /// <summary>
        /// Содержимое элемента (необязательно).
        /// </summary>
        string Content { get; set; }
        
        /// <summary>
        /// Текстовое содержимое элемента (к примеру, текст внутри ссылки).
        /// </summary>
        string TextContent { get; set; }
        
        /// <summary>
        /// Дочерние элементы.
        /// </summary>
        IEnumerable<IElement> Children { get; }

        IEnumerable<string> Classes { get; }
        
        IDictionary<string, string> Attributes { get; }

        /// <summary>
        /// Получить коллекцию элементов по имени класса
        /// </summary>
        /// <returns>Коллекция элементов</returns>
        IEnumerable<IElement> GetElementsByClassName(string className);

        /// <summary>
        /// Получить коллекцию элементов по тегу
        /// </summary>
        /// <returns>Коллекция элементов</returns>
        IEnumerable<IElement> GetElementsByTagName(string tagName);

        /// <summary>
        /// Получить значение атрибута.
        /// </summary>
        /// <param name="attribute">Атрибут.</param>
        /// <returns>Значение атрибута.</returns>
        string GetAttributeValue(string attribute);
    }
}