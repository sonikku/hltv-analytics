using System;

namespace HltvParser.Core.Exceptions
{
    /// <summary>
    /// Выбрасывается, если внутренний элемент равен null
    /// </summary>
    public class InnerElementNullException : Exception
    {
        public InnerElementNullException() : base() {}
        
        public InnerElementNullException(string message) : base(message) {}
    }
}