﻿using System;

namespace HltvParser.Core.Exceptions
{
    public class ContextException : Exception
    {
        public ContextException(string message, Exception innerException = null) : base(message, innerException) {}
    }
}