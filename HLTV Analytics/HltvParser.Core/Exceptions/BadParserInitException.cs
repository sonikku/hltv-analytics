﻿using System;

namespace HltvParser.Core.Exceptions
{
    /// <summary>
    /// Being throwed when user tries to init parser with wrong settings
    /// </summary>
    internal class BadParserInitException : Exception
    {
        public BadParserInitException() : base() {}
        
        public BadParserInitException(string message) : base(message) {}
    }
}