﻿using System;

namespace HltvParser.Core.Exceptions
{
    public class BadUrlException : Exception
    {
        public BadUrlException() : base() {}
        public BadUrlException(string message) : base(message) {}
    }
}