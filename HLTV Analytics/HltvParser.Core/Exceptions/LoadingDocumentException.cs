﻿using System;

namespace HltvParser.Core.Exceptions
{
    public class LoadingDocumentException : Exception
    {
        public string Url { get; private set; }
        
        public LoadingDocumentException() : base() {}
        public LoadingDocumentException(string message) : base(message) {}

        public LoadingDocumentException(string url, string message = null) : base(message)
        {
            Url = url;
        }
    }
}