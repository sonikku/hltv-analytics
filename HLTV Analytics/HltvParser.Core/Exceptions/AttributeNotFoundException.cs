using System;

namespace HltvParser.Core.Exceptions
{
    public class AttributeNotFoundException : Exception
    {
        public string Attribute { get; }
        
        public AttributeNotFoundException(string attribute) 
            : base("Couldn't find attribute " + attribute)
        {
            Attribute = attribute;
        } 
    }
}