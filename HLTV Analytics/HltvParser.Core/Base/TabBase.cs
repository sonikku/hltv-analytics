namespace HltvParser.Core.Base
{
    public class TabBase
    {
        protected TabConfig Config { get; }

        public string Address { get; protected set; } = "";
        
        #region Конструкторы
        
        public TabBase()
        {
            // Стандартный конфиг
            Config = new TabConfig()
            {
                AllowNavigation = true,
                UseCookies = true,
                UseJavaScript = true
            };
        }
        
        public TabBase(TabConfig config)
        {
            Config = config;
        }
        
        #endregion
    }
    
    public class TabBase<TTab, TDocument, TElement> : TabBase
    {
        protected TTab InnerTab { get; set; }
        
        /// <summary>
        /// Веб-страница
        /// </summary>
        public DocumentBase<TDocument, TElement> Document { get; protected set; }
    }
}