using System.Collections.Generic;
using System.Linq;
using HltvParser.Core.Interfaces;

namespace HltvParser.Core.Base
{
    public static class ElementExtensions
    {
        public static bool TryGet(this IEnumerable<IElement> elements,
            int index, 
            out IElement element)
        {
            element = elements.ElementAtOrDefault(index);
            return element != null;
        }

        public static bool TryGetFirst(this IEnumerable<IElement> elements,
            out IElement element)
        {
            return elements.TryGet(0, out element);
        }
    }
}