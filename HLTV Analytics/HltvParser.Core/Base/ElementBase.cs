using System.Collections.Generic;
using HltvParser.Core.Interfaces;

namespace HltvParser.Core.Base
{
    /// <summary>
    /// Элемент веб-страницы
    /// </summary>
    public abstract class ElementBase : IElement
    {
        /// <summary>
        /// Название элемента (тег). Например, body, img, a, p, h1 и так далее.
        /// </summary>
        public abstract string Name { get; }
        
        /// <summary>
        /// ID элемента (если есть).
        /// Стандарт HTML требует уникальности ID элемента, но данное условие не
        /// выполняется гарантированно.
        /// </summary>
        public abstract string Id { get; set; }
        
        /// <summary>
        /// Название класса элемента (если есть). Если имеется несколько
        /// классов, то они все будут перечислены через пробел.
        /// </summary>
        public abstract string Class { get; set; }
        
        /// <summary>
        /// Содержимое элемента (необязательно).
        /// </summary>
        public abstract string Content { get; set; }

        /// <summary>
        /// Текстовое содержимое элемента.
        /// </summary>
        public abstract string TextContent { get; set; }

        /// <summary>
        /// Дочерние элементы.
        /// </summary>
        public abstract IEnumerable<IElement> Children { get; }

        public abstract IEnumerable<string> Classes { get; }
        
        public abstract IDictionary<string, string> Attributes { get; }

        /// <summary>
        /// Получить коллекцию элементов по имени класса
        /// </summary>
        /// <returns>Коллекция элементов</returns>
        public abstract IEnumerable<IElement> GetElementsByClassName(string className);

        /// <summary>
        /// Получить коллекцию элементов по тегу
        /// </summary>
        /// <returns>Коллекция элементов</returns>
        public abstract IEnumerable<IElement> GetElementsByTagName(string tagName);

        /// <summary>
        /// Получить значение атрибута.
        /// </summary>
        /// <param name="attribute">Атрибут.</param>
        /// <returns>Значение атрибута.</returns>
        public abstract string GetAttributeValue(string attribute);

        /// <summary>
        /// Попробовать получить значение атрибута.
        /// </summary>
        /// <param name="attribute">Атрибут.</param>
        /// <param name="value">Значение атрибута.</param>
        /// <returns>Истина, если удалось получить атрибут.</returns>
        public abstract bool TryGetAttribute(string attribute, out string value);
    }

    public abstract class ElementBase<TElement> : ElementBase
    {
        protected TElement InnerElement { get; set; }
        
        public ElementBase(TElement element)
        {
            InnerElement = element;
        }
    }
}