using System.Collections.Generic;
using HltvParser.Core.Interfaces;

namespace HltvParser.Core.Base
{
    public abstract class DocumentBase<TElement>
    {
        public abstract IEnumerable<ElementBase<TElement>> Elements { get; }
        
        public abstract string Title { get; }

        /// <summary>
        /// Получить элемент по его уникальному ID
        /// </summary>
        /// <param name="id">ID элемента</param>
        /// <returns>Элемент</returns>
        public abstract IElement GetElementById(string id);

        public abstract IEnumerable<IElement> GetElementsByClassName(string className);

        public abstract IEnumerable<IElement> GetElementsByTagName(string tagName);
    }

    public abstract class DocumentBase<TDocument, TElement> : DocumentBase<TElement>
    {
        internal TDocument InnerDocument { get; set; }

        protected DocumentBase(TDocument document)
        {
            InnerDocument = document;
        }
    }
}