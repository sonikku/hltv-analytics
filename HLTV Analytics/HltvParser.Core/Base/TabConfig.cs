﻿namespace HltvParser.Core.Base
{
    /// <summary>
    /// Конфигурация вкладки
    /// </summary>
    public class TabConfig
    {
        /// <summary>
        /// Использовать JS
        /// </summary>
        public bool UseJavaScript { get; set; }
        
        /// <summary>
        /// Позволить навигацию
        /// </summary>
        public bool AllowNavigation { get; set; }
        
        /// <summary>
        /// Использовать cookies
        /// </summary>
        public bool UseCookies { get; set; }
    }
}