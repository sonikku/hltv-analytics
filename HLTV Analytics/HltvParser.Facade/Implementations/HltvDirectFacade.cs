﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HltvParser.Api.Contexts;
using HltvParser.Api.Contexts.Base;
using HltvParser.Facade.Interfaces;
using HltvParser.Models.Interfaces.Analytics;
using HltvParser.Models.Interfaces.Match;
using HltvParser.Models.Interfaces.Team;
using HltvParser.Models.Match;

namespace HltvParser.Facade.Implementations
{
    public class HltvDirectFacade : IHltvParserFacade
    {
        public async Task<IAnalytics> GetAnalyticsData(string url)
        {
            var context = new AnalyticsContext(url);
            return await context.RetrieveDataAsync();
        }

        public async Task<IMatch> GetMatchData(string url)
        {
            var context = new MatchContext(url);
            return await context.RetrieveDataAsync();
        }

        public async Task<UpcomingMatch> GetUpcomingMatchData(string url)
        {
            var context = new UpcomingMatchContext(url);
            return await context.RetrieveDataAsync();
        }

        public async Task<IEnumerable<IMatch>> GetMatchesList()
        {
            var context = new MatchesListContext();
            return await context.RetrieveDataAsync();
        }

        public async Task<IEnumerable<ITeam>> GetRankings()
        {
            var context = new RankingsContext();
            return await context.RetrieveDataAsync();
        }

        public async Task<IEnumerable<IMatch>> GetResults(int offset = 0)
        {
            var context = new ResultsContext(offset);
            return await context.RetrieveDataAsync();
        }
    }
}