﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HltvParser.Models.Interfaces.Analytics;
using HltvParser.Models.Interfaces.Match;
using HltvParser.Models.Interfaces.Team;
using HltvParser.Models.Match;

namespace HltvParser.Facade.Interfaces
{
    public interface IHltvParserFacade
    {
        Task<IAnalytics> GetAnalyticsData(string url);
        Task<IMatch> GetMatchData(string url);
        Task<UpcomingMatch> GetUpcomingMatchData(string url);
        Task<IEnumerable<IMatch>> GetMatchesList();
        Task<IEnumerable<ITeam>> GetRankings();
        Task<IEnumerable<IMatch>> GetResults(int offset = 0);
    }
}